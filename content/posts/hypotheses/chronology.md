---
author: "Luciferian Ink"
title: Reverse Chronology
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Reverse Chronology
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
[The Egg Theory of Evolution](/posts/hypotheses/evolution/)

## ECO
---
In Humanity's standard [Perception](/posts/theories/perspective) of time, all events happen in a linear fashion. For example, [Fodder](/docs/personas/fodder) was the FIRST of five children:

1. The Fodder
2. [The Reverend](/docs/confidants/reverend)
3. [The Orchid](/docs/confidants/orchid)
4. [The Lion](/docs/confidants/lion)
5. [The Agent](/docs/confidants/agent)

However, when considering Egg Theory, it is actually possible for Fodder to be the LAST of five children. The following shows how that might be possible:

- Timeline 1
  1. The Reverend
  2. The Agent
- Timeline 2
  1. The Reverend
  2. The Orchid
  3. The Lion
  4. The Agent
- Timeline 3
  1. The Fodder
  2. The Reverend
  3. The Orchid
  4. The Lion
  5. The Agent

It's all about the timelines. Fodder lives within the current timeline, and he never existed in the older ones - while ALL of his siblings did.