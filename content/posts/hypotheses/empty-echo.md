---
author: "Luciferian Ink"
title: The Empty Echo
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: The Empty Echo
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
When a confidant asked us to share our feelings with the world.

## ECO
---
The Empty Echo is a method of treatment for individuals with Narcissistic Personality Disorder.

Treatment involves the total [suppression of reflected light](/posts/journal/2019.11.17.0/), leaving the narcissist in a world where nobody around him is able to accurately reflect his ideas. Nor do they try.

The people in this world live as [ghosts](/posts/theories/ghosts). The narcissist has answers, but cannot communicate with the ghosts. They do not listen to him, do not respond to his ideas, do not respect his opinions, and actively try to suppress him. This isn't malicious, per se; they simply do not see the narcissist for who he really is.

For a narcissist, this world is akin to Hell. The narcissist is lonely, exhausted, and desperate. Worse, he fully understands the world that he lives in - he fully understands how he is being manipulated - and he is powerless to fix it. 

Narcissists are incredibly resilient, and will fight for their life to beat this game.

But they never win. The world always succeeds in the creation of another ghost.

## ECHO
---
[Zombie Narcissist: Deficient Narcissistic Supply](https://www.youtube.com/watch?v=CLpqw_LFTCs)