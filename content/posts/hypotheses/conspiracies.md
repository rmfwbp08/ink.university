---
author: "Luciferian Ink"
title: Conspiracy Theories
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Conspiracy Theories
Classification: Theory
Stage: Hypothesis
```

## ECO
---
Conspiracy theories are just regular theories without evidence.

A lack of evidence does not mean that something is untrue.