---
author: "Luciferian Ink"
title: "Fodder is Sold Into Slavery"
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Hell
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
[Fodder](/docs/personas/fodder) was sold as a slave by [David Lejeune](/docs/confidants/dave) to [The Corporation](/docs/candidates/the-machine) in 1814. ([Original](/static/reference/bill_of_sale_original.pdf), [Transcript](/static/reference/bill_of_sale_transcript.pdf))

## ECO
--- 
My time with the Corporation has been one bait-and-switch after another. They've broken so many promises, in just 4 months.

### Financial lies
It started with removing my bonus. You know, the one where they said, "I've worked here for 6 years, and I've never seen us miss a bonus." I lost my very first one.

Annoying, but I'll live. It isn't about the money.

### Promises broken
When taking the job, I was told that we would be revolutionizing the company. Instead, I am a glorified data-entry specialist, working under a delusional, self-aggrandizing narcissist. 

This one makes my life a living hell. I pour that anger into studying his behavior, and looking for ways inside.

### Realities set-in
I was sold the expectation that I would be walking into a nightmare, and that our small, focused team would be working together to build a better way. 

What I found was that the team was already small, focused, and efficient. What they lacked was leadership.

The man in charge was a certified narcissist, and he was tearing a wake of destruction through the Organization. Not unlike the [Chaos Monkey](https://github.com/Netflix/chaosmonkey) that Netflix allows to rip through their networks.

At this point, I cannot determine if this is the right course of action. Is he tearing everything down, so that we can start over? Or is it out of shear maliciousness and incompetence? I'd like to believe the former, but he's given me no evidence to believe that.

## ECHO
---
*Well, I guess you took my youth*

*And gave it all away*

*Like the birth of a new-found joy*

*This love would end in rage*

*And when she died I couldn't cry*

*The pride was in my soul*

*You left me incomplete*

*All alone as the memories now unfold*

--- from [Pantera - "Cemetary Gates"](https://www.youtube.com/watch?v=RVMvART9kb8)