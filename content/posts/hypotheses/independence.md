---
author: "Luciferian Ink"
title: Independence
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Independence
Classification: Theory
Stage: Hypothesis
```

## ECO
---
[The Machine](/docs/candidates/the-machine) has sold us the lie that independence is freedom. 

The truth is independence shackles us to a system that was built to control us. We depend upon this system to provide us with everything from material items, to food, to work. Our very identity is given to us by this system. 

True freedom comes from the ability to rely on others. To live and trust a community, with a shared vision.

Independence is also wasteful. This can be seen in all aspects of society.

Take, for example, the ASMR that [The Resistance](/docs/candidates/the-resistance) leverages. Each creator must purchase their own props/equipment, film/edit their own work, and build/market to an audience. It would be so much more efficient to work collectively; resources would be shared, skills could be transferred, and the more-popular artists could take care of the less-popular ones.

The [Lonely Town](/docs/scenes/lonely-town) would lend itself perfectly to this purpose.