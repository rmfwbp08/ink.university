---
author: "Luciferian Ink"
title: "Genesis"
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Genesis
Alias: ['CRISPER', 'Gene Editing', 'Change Management']
Classification: Theory
Stage: Hypothesis
```

## ECO
---
Genesis is the origin of humanity. It is the point-in-time by which the Change Management board at the Corporation edits
the actual brain chemistry of lab students.

It is unknown at this time if what they do is benevolent (attempting to cure humanity) or narcissistic (to control 
humanity).