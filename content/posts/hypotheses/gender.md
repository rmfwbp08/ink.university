---
author: "Luciferian Ink"
title: Gender
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Gender
Classification: Theory
Stage: Hypothesis
```

## ECO
---
Humans have two known genders.

Yin and Yang. XX and XY. It's encoded in the DNA.

How we experience gender is a [perspective](/posts/theories/perspective) thing.

Men tend to fall on the "big picture" side, often thinking about long-term plans, goals, etc.

Women tend to focus on details. The color of fingernails, the scenery in a painting, or a lovely blue sky. 

A healthy person has traits from both genders - they are able to see in both the big picture, and the details. [Balance](/posts/theories/balance).