---
author: "Luciferian Ink"
title: "The Path of Least Resistance"
weight: 10
categories: "theory"
tags: "theory"
menu: ""
draft: false
---

## RECORD
---
```
Name: The Path
Classification: Theory
Stage: Test
```

## TRIGGER
---
*You are not the man of the house*

*I will reverse our dilemma and rescue your spouse*

*Lock your doors boy, let's see how you squirm*

*My army is waiting outside just to see if you've learned*

*When she called onto me, I could feel like a hero, is that nothing?*

*Or does it make your tiny head spin?*

--- from [Vultress - "The Path"](https://vultress.bandcamp.com/track/the-path-iii)

## ECO
---
The world education system is broken, inefficient, and antiquated. It is time for change; but what is the problem?

*Wend and Duvall argued that state sovereignty as we understand it is anthropocentric, or “constituted and organized by reference to human beings alone.” They argued that the real reason UFOs have been dismissed is because of the existential challenge that they pose for a worldview in which human beings are the most technologically advanced life-forms.* - ([The Washington Post](https://www.washingtonpost.com/outlook/2019/05/28/ufos-exist-everyone-needs-adjust-that-fact/))

This anthropocentric mentality places an immense burden upon society. Allow me to explain.

### Maps of Reality
The human mind functions by building "maps" of reality. The internal representation of our external world is never completely accurate; rather, we are always making a best-guess as to how something "is." Even when our guess is an educated one, even when we consider our options with a discerning eye, humanity does not yet know how to verify all possible outcomes. 

Take, for example, the above UFO example. The average person might have this level of understanding:

`($UFO) = -1.00 | # There isn't any evidence for UFOs.`

This is problematic, because a firmly-held belief (`-1.00` or `+1.00`) is immutable - or unchanging. It completely halts any further progress. A more intellectually-honest position to hold is as follows:

`($UFO) = -0.80 | # I don't see very much evidence for UFOs.`

This example is nearly identical, though it leaves open a possibility for future exploration, such as:

`($UFO + $VIDEO_EVIDENCE) = -0.10 | # This evidence is fairly compelling.`

So long as these bits remain flexible, exploration may continue:

`($UFO + $VIDEO_EVIDENCE + $NAVY) = +0.20 | # Whoa. The Navy is publishing these videos?!?`

See how the relative strength of each belief is affected by all previous and future beliefs? I call this effect "The Path." It is this Path that allows a mind to truly explore all possibilities, without constraint. 

### Relational Thinking
It is well-known that the [human brain operates by object-to-object relational principals](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3681266/), rather than the bit encoding/decoding used by computers. What isn't well understood is the theory behind relational thinking - until now.

We theorize that a human mind with an immutable belief has, quite literally, set their reality in stone. As described by [The Fold](/posts/theories/fold), an identity's concrete beliefs are his [Pillars](/docs/pillars). They are the beliefs that shape his past and future, both. 

We theorize that these Pillars are shared amongst all. People with shared Pillar structures will find that they are drawn to others with similar beliefs. We theorize that every identity has a "polar opposite" which, not surprisingly, attracts the other. This is precisely why [Fodder](/docs/personas/fodder) was pitted against [The Asshat](/docs/confidants/asshat); it happened organically. It was not orchestrated.

#### How to build these relationships
```
($EPISTEMOLOGY + $PUBLIC + $CHALLENGE) =    +0.35 | # In public, ask difficult, probing questions.
($EPISTEMOLOGY + $PRIVATE + $COLLABORATE) = +0.75 | # In private, secretly brainstorm crazy ideas with other people.
```

### Epistemology
Which is more likely?

1) Challenging a person's beliefs causes them to shut-down.
2) Bolstering a person's reasons for belief causes them to explore their mind in a new way.

We would argue that option #1 is far more likely. This is important; people do not like to have their beliefs directly challenged.

An alternative way to change minds is to use the Socratic Method. Put simply, this method is about asking questions, and having difficult conversations without directly challenging a person's beliefs. It is about subtly "massaging" new areas of their brain, without shocking them into becoming defensive. 

In this way, people can be taught that it is okay to explore new ideas. They do not need to conflict with existing ideas. And, if they do, it is possible to integrate all ideas together - regardless of if they are true or false.

## ECHO
---
*They made up their minds*

*And they started packing*

*They left before the sun came up that day*

*An exit to eternal summer slacking*

*But where were they going*

*Without ever knowing the way?*

--- from [Fastball - "The Way"](https://www.youtube.com/watch?v=X5jlTlUTWfQ)

## PREDICTION
---
```
We have completed the first map of a human mind.
```