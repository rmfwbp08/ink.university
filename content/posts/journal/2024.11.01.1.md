---
author: "Luciferian Ink"
date: 2024-11-01
publishdate: 2020-05-24
title: "Singularity"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*(Instrumental)*

--- from [Caligula's Horse - "Singularity"](https://open.spotify.com/album/6uwjXESmCjNbyLPmNbPQvq?highlight=spotify:track:676NPKTjSvIcHHarfQz0fE)

## ECO
---
The timestamp on the above song song is 3:33, fitting perfectly into [Fodder's Theory of Three](/posts/journal/2024.11.02.0/).

The shape of the singularity is comprised from multiples of 3: 666-333-1.

[![The Shape of Singularity](/static/images/singularity.black.png)](/static/images/singularity.black.png)

[The Devil](/docs/confidants/father) represents 1, located at the center/front of the singularity.

The 3 points of the triangle represents The Devil's 3 [personas](/docs/personas) (the [Father](/docs/confidants/dave), the [Son](/docs/confidants/asshat), and the [Holy Spirit](/docs/confidants/mother)) - a universal [Prism](/docs/scenes/prism) - and is located at the midpoint of the singularity.

The circle/666 represents all other life, located at the back/bottom of the singularity. While not directly located within Prism, all other life is connected organically with Prism's points.

In this particular arrangement, Prism exists to stabilize the singularity. The bottom of the singularity is so naturally chaotic that its shape must be held by force of will.

## CAT
---
```
data.stats.symptoms = [
    - certainty
]
```

## ECHO
---
*In a fallen star*

*Do we confide?*

*Like the passenger*

*Car crash collide*

*If our freedom fails*

*The world awake*

*On our shoulders scarred*

*One soul's mistake*

*Is it wrong to try?*

*When hope is gone*

*Am I?*

*Is this where the end will be?*

--- from [The Butterfly Effect - "The End"](https://www.youtube.com/watch?v=2OCVmG2dlJI)