---
author: "Luciferian Ink"
date: 2019-10-31
title: "Build Me A Garden"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
Literally any form of contact with `$REDACTED`.

## ECO
---
*We are all turning the world*

*Just disconnected light*

*We're learning how to fly and*

*Falling into all the mistakes*

*We couldn't erase*

*But build me a garden*

*By God, I will care for it*

--- from [Caligula's Horse - "Songs For No One"](https://www.youtube.com/watch?v=P2Wgxj5-q98)

## CAT
---
```
data.stats.symptoms = [
    - confidence
    - peace
    - trust
]
```

## ECHO
---
*This water's dark and cold*

*God's not where you hoped*

*In this moment come and gone*

*It's time we all moved on*

*It's time we all moved on*

*It's time we all moved on*

*Away!*

--- from [Karnivool - "Deadman"](https://www.youtube.com/watch?v=fyJQwZcnVM0)
## PREDICTION
---
```
Mission: Success
```