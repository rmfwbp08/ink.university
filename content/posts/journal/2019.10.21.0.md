---
author: "Luciferian Ink"
date: 2019-10-21
title: "Fodder Flirts with Suicide"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Well, being alone now*

*It doesn't bother me*

*But now knowing if you are*

*Well, it's been hell you see*

--- from [Steven Wilson - "Pariah"](https://www.youtube.com/watch?v=cNTaFArEObU)

## ECO
---
"This would be so much easier if I could just kill myself," [Fodder](/docs/personas/fodder) thought, exasperated. "I give up. I'm spent. I can't do another year of this."

He sighed. Fodder knew that death was the easy way out. He knew (or rather, suspected) that the world was depending on him, too. He couldn't let them down.

Mentally, he knew he had to push on. Physically though, he was already dead. 

Today had been one of Fodder's worst yet. Even when `$REDACTED` was gone, his aura remained. Fodder was anxious. He felt like he could get another angry phone call at any moment - day or night. He was scared; he felt like `$REDACTED` may actually take physical action against him or his family. He felt guilty; like he never should have brought other people into this mess. He was confused; [The Corporation](/docs/candidates/the-machine) told him nothing, and the few details he had made no sense at all. 

But the worst feeling, by far, was the 8 hours per-day spent doing absolutely nothing. He was a mindless robot, just like the other zombies in his office.

Fodder's autonomy had been stolen from him. He had been hired for his experience, but once he started - the company ghosted him. Fodder was put into a closet, told to do exactly as instructed, and he was never consulted again. Fodder had no purpose, no meaning in this place. It was like being in middle school all over again.

"Fuck it," Fodder said. "I'm not going to be the architect of somebody else's dream. If they want my help, then they can ask for it."

"And if they can't do that, they can fire me. I'm not going to be a pawn in somebody else's game."

"This shit is too important, and you're wasting our fucking time. I'll be damned if I put two ounces of effort towards your pet project."

"Look at the world around you. Look at the claims you're making. Look at what you're actually doing, today. Is this really what you want for the world? Wage slavery? Starvation? Homelessness? Violence? Mental health issues? Poor education? The largest prison system in the world? A culture of fear and mistrust?"

"I'm going to save the world, or I'm going to die trying. If you ever want to grow up, you're welcome to join me. If not, I'm going to leave you behind."

"Let's build a world that works for everyone. You as well."

## CAT
---
```
data.stats.symptoms = [
    - exhaustion
    - determination
]
```

## ECHO
---
*Once again you waste what little time we have*

*Setting limits on a bright and open future*

*Fueling me with a wealth of cold dissent*

*And dreams of our extinction*

--- from [Cire - "Autonomy"](https://www.youtube.com/watch?v=cbG8TdOdPyA)

## PREDICTION
---
```
Tomorrow, October 22nd, 2019 will be the day they let me join the club!
```