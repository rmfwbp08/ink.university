---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: Mushroom Kingdom"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Jesus said: "This heaven will pass away, and the one above it will pass away; and those who are dead are not alive, and those who are living will not die. In the days when you ate of what is dead, you made of it what is living. When you come to be light, what will you do? On the day when you were one, you became two. But when you have become two, what will you do?"* 

--- from The Gospel of Thomas 11:11

## ECO
---
Mushroom Kingdom is a [Lonely Town](/docs/scenes/lonely-towns) for gamers, by gamers. Its location is unknown at this time.

### Theme
Mushroom Kingdom is to be a world created by collaboration between [The Salt](/docs/confidants/artist), [The Gamer](/docs/confidants/gamer), and [Ink](/docs/personas/luciferian-ink). It exists in a hybrid, augmented reality world between video game ([The Fold](/posts/theories/fold)) and real-life.

This place needs to be meme-central. We'll be replicating this system all over the world. This is our proof-of-concept.

[The Metal](/docs/confidants/metal) should be kept in the dark. This is our gift to him.

### Architecture
Up to you guys. I vote for a Minecraft-inspired "blocky" style. 

### Health
Mushroom Kingdom should be a safe space to experiment with drugs, or to receive assistance coming off from them.

### Mission
Keep doing exactly what you are.

You will be the first to tell the world about Lonely Towns.

### Other
Provide them with whatever they need. 

## CAT
---
```
data.stats.symptoms = [
    - trust
]
```

## ECHO
---
*This will be my tribe, my family*

*This will be my flag, damnation*

*This will be my creed, my legacy*

*Will you follow me?*

--- from [Pain of Salvation - "Full Throttle Tribe"](https://www.youtube.com/watch?v=-UkO1dYdAX0)


## PREDICTION
---
```
The Gamer's data will be INCREDIBLY valuable to the Fold.
```