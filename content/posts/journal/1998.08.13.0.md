---
author: "Luciferian Ink"
date: 1998-08-13
title: "Pure Insanity"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A symbol.

## ECO
---
### A Brainwashing
[Fodder's](/docs/personas/fodder) mother, [The Queen](/docs/confidants/mother), would spend most of his childhood indoctrinating her children with religious ideology. Core among these beliefs was the concept of "sexual purity," or, the idea that one should wait until marriage before having sex. 

For the most part, Fodder adopted this mentality, and ran with it. It became a core part of his identity. It became a core factor that he looked for in women.

It would become a core reason why he could never find anyone, too. And why he didn't stay with the ones he did find.

### A Wedding
Fodder would grow out of these beliefs in his early 20's, along with many others he had been brainwashed with. A human being's worth is not tied to their purity, he had come to accept. 

But at this point, he was so broken and lonely that the dating didn't follow. He lived in shame of his past beliefs, and current failings. Nobody would ever want to be with a person like him, he thought.

Fodder was horrified to see this brainwashing reappear at the wedding of his brother, [The Orchid](/docs/confidants/orchid) and [The Scientist](/docs/confidants/scientist). While standing at the altar, giving a speech to the audience, The Scientist's father found it necessary to speak to his daughter's purity, and how the two had waited until marriage.

"Disgusting," Fodder thought, "He has no business talking about this in front of a crowd. The Scientist's purity has nothing to do with who she is as a person. This programming is just so damaging to society. This is EXACTLY where sexual predators come from."

Worse, Fodder is ashamed to admit, is that he secretly longed for someone who had waited for him.

### A Problem
`$REDACTED`

## CAT
---
```
data.stats.symptoms = [
    - anger
    - shame
    - disgust
]
```

## ECHO
---
*Little Suzy, she was only twelve*

*She was given the world*

*With every chance to excel*

*Hang with the boys and hear the stories they tell*

*She might act kind of proud*

*But no respect for herself*

*She finds love in all the wrong places*

*The same situations*

*Just different faces*

*Changed up her pace since her daddy left her*

*Too bad he never told her*

*She deserved much better*

--- from [P.O.D. - "Youth of the Nation"](https://www.youtube.com/watch?v=BuHNDaFFO_0)