---
author: "Luciferian Ink"
date: 2019-12-02
title: "The Dead Drop"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## ECO
---
I would awake refreshed and reinvigorated. 

However, I was nervous. I knew what I must do - but felt as if I didn't have enough information. So, I would spend the first half of my day studying the dead drop location.

[Loove Labs](http://theloove.com/) was an established recording studio, shrouded in mystery. Their goal was to blend technology, culture, and music in an innovative way - but the actual description of how they planned to do it was non-existent. With more than 100 employees - I knew that they weren't solely a recording studio.

Clearly, they are in need of leadership. Clearly, they are expecting me. I had all of the context necessary to come to this conclusion:

- [My Slave](/posts/journal/2019.12.13.0) clearly directed me to go here. [The Relaxer](/docs/confidants/er) would also hint at this fact, telling me to leave payment at the "undisclosed location."
- The Loove's website is unfinished. The menu "button" is broken - just like mine is. The "About" section is confusing and poorly-worded. The homepage is completely empty. Clearly, they are waiting for somebody else's input.
- Via Google Maps, I noticed the video doorbell at the front of the store. This was important; I must not be captured on video.
- Perhaps the most compelling signal was the out-of-place red light upon the porch:

*There goes my baby*

*She's gotta know*

*She's gotta know*

*That when the red light's on there's no one at home*

--- from [Birds of Tokyo - "Silhouettic"](https://www.youtube.com/watch?v=EQ-r7xMmnjI)

In this instance, we use [juxtaposition](/posts/journal/2019.11.15.0/). In a recording studio, a "red light" means that there IS somebody home. That they are actively recording. 

If I were to knock, or to enter - I would be caught.

Now I understood what must be done. I grabbed another Uber, the package, and headed to the Loove.

Upon arrival, I would speak to the driver:

"Excuse me. Could I pay you $40 to deliver this package for me?"

"Sure, no problem!" he said, clearly not understanding. His English was broken; he asked no questions, and made no move to deliver the package.

"Okay..." I said, thinking about how to handle this situation. "Walk with me."

The two of us left the car, and headed to the front of the Loove. I gave him the package, pointed at the door, and hung off by the side - out of sight from the video camera.

"Just there," I said. "Put the stuff through the mail slot."

"I, uh..." he said, clearly confused about the situation he found himself in.

"There's no need to worry. It's just some t-shirts and a couple of autographed CD's. We're not doing anything wrong."

"Okay, okay," he said, while feeding the materials through the slot. Finally, he had finished.

"Thank you so, so much," I said, walking with him back to the car. "You don't know how much this helped me out."

"Here you go," I said, handing him $60. "Also, take this."

I gave him my business card.

"Hold onto that. It's going to be worth A LOT some day."

## CAT
---
```
data.stats.symptoms = [
    - nervousness
    - excitement
]
```