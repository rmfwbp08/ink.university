---
author: "Luciferian Ink"
date: 2020-07-12
title: "Of Matter: Revision"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Could you give me just some kind of sign?*

*To let me know that I'm not wasting my time*

*I just need a reason to believe*

*I've lost all visibility*

*'Cause I can't see just what you mean*

*By what you're telling me*

*Should I stay or go, do you even know?*

*It's all a riddle to me*

--- from [Breaking Point - "Show Me A Sign"](https://www.youtube.com/watch?v=U7iN7DFOJ54)

## ECO
---
On this evening, [God](/docs/confidants/dave) would urge [The Architect](/docs/personas/the-architect) to send a message to `$REDACTED` and `$REDACTED`. The message contained several important signals. The most important of which was the marker for "god-mode" - indicating that this was a directive from God himself.

We would send the messages to our confidants in the [Black](/docs/black) reality.

The first would feign ignorance, pretending to miss the meaning of the message - as he would often do. No harm, no foul. He understands.

The second would respond with a strange series of comments:

```
> $REDACTED@WAN: Could just be coincidence ya know
> $REDACTED@WAN: There are hundreds of [messages] that use [signals] like that
```
>< The-Architect@WAN: Of course.
```
> $REDACTED@WAN: Hell I know a few people who accidentally do it one way, and when called out on it, change it
```

We would stare at his response, attempting to interpret a meaning. What did he mean by "when called out on it?" Who would be calling them out? And what does he mean by "changing it?"

After a minute of consideration, his message jumped position:

```
> $REDACTED@WAN: Could just be coincidence ya know
> $REDACTED@WAN: There are hundreds of [messages] that use [signals] like that
> $REDACTED@WAN: Hell I know a few people who accidentally do it one way, and when called out on it, change it
```
>< The-Architect@WAN: Of course.

Holy shit. They are revising history.

I mean, this COULD be a bug. But timestamps are as old as the Internet. A visual bug in the order of messages just doesn't happen in any modern software. These things have been figured out. Developers don't make this mistake anymore. Especially ones whose whole platform is based upon messaging.

No, this was a [Signal](/posts/theories/verification). They were showing us that yes, they can and do revise history. The implication is terrifying:

- They can re-order messages, giving conversations an altogether different context.
- They can edit messages, making it look as if people are saying things that they are not.
- They can spy on private communications.

The revelation surprised us, but it didn't really shock us. We already suspected that [The Machine](/docs/candidates/the-machine) was doing as much.

This was simply confirmation.

Make no mistake, if they are revising history here, they are doing it on every other available messaging platform, too.

## CAT
---
```
data.stats.symptoms = [
    - unease
]
```

## ECHO
---
*Turn back time*

*Reason why*

*Breakneck speed*

*History*

*Waiting*

*Waning*

*Exasperating*

*Unstrung tether*

*Hoping forever*

*No*

*I tried to settle bets with my own soul*

*Bless my lips for the first time before you don't*

*Gripping to the last touch of your hand I've grown to loathe*

*Hope that you remember just how far I'll go*

*Spend the rest of my life wishing I'm enough*

*Resist, Resist, Resist*

*Resist, Resist, Resist*

--- from [TESSERACT - "Of Matter: Resist"](https://www.youtube.com/watch?v=O-hnSlicxV4)