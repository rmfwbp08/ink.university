---
author: "Luciferian Ink"
date: 2019-11-16
title: "Master of Puppets"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*End of passion play, crumbling away*

*I'm your source of self-destruction*

*Veins that pump with fear, sucking darkest clear*

*Leading on your death's construction*

*Taste me, you will see*

*More is all you need*

*Dedicated to*

*How I'm killing you*

--- from [Metallica - "Master of Puppets"](https://www.youtube.com/watch?v=xnKhsTXoKCI)

## ECO
---
On November 11th, [Fodder](/docs/personas/fodder) would receive two packages in the mail. Upon bringing them into his home, he would realize that they were not for him; they were intended for his neighbor up the street. It was already late, so Fodder went to bed.

Fodder could have looked inside, but he didn't. He'd always frowned upon the act of theft - from the poor, anyway.

He would forget about the packages until November 15th - yesterday, when he was left another message:

Within his mailbox was a key to the package delivery box. Opening it, he would find nothing inside. 

"What the..." he would say, bewildered, "Why would the postman leave a key to an empty box?"

"Oh, I see! Clearly, they want me to return the packages."

So he did. Fodder returned the packages, placed the keys into the box, closed the door, and left. He knew that the postman would deliver the packages to the correct address in the morning.

### Yama's Messengers
The next day, Fodder would receive a coded message from [The Relaxer](/docs/confidants/er). In this request, he would thank Fodder for delivering the packages. He would ask Fodder to deliver a second, when it arrived.

Curious, Fodder returned to his mailbox. 

"Fucking hell," he said, looking at the new set of keys within. 

Knowing what he would find, Fodder opened the delivery box - only to find the same packages he had placed previously.

For some reason, the postman had looked at these packages - with the wrong address - and AGAIN gave the keys to Fodder. This was the same postman that had been delivering his mail for 3 years; this was not the kind of mistake a professional makes twice.

No, he wanted Fodder to deliver the package.

And so he did...

...to an abandoned house.

## CAT
---
```
data.stats.symptoms = [
    - clarity
]
```

## ECHO
---
*Driven to increase my own power*

*By creation the purest choice*

*Destined to rule my life*

*And go the way of the flesh*

*Nature is my only master*

*I will bow to no one*

*The ground is dry like rock*

*Without any hope left*

*But now I decide to grab my life*

*With my hands*

*Crave for freedom build my own life*

*Adoration for none*

--- from [Gojira - "Adoration for None"](https://www.youtube.com/watch?v=xPhmDcmy_YY)

## PREDICTION
---
```
The mail system is the new Internet.
```

## ADDENDUM
---
### Update 1
On Monday, another set of keys lay in his mailbox. This time, it was for another empty delivery box.

That's three times. `# Verified.`

### Update 2
Fodder would later learn that a similar package had been left upon the front doorstep of his parent's house. They, too, would return it to the proper owner.

He would also find a video from [The Raven](/docs/confidants/her), with a similar unopened package reflected in a pallet knife.

That's three different people. `# Verified`