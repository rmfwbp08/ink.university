---
author: "Luciferian Ink"
date: 2019-12-03
title: "The Set-Up"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## ECO
---
It was late when I arrived in Buffalo. It was dark, and it was snowing heavily. The roads were a slushy, frozen mess. I was tired, and nervous.

The first thing I did was replace my cell phone. I could no longer trust Google; they had proven time and again to be abusing their power:

- They were clearly looking at the images in my Google Photos account.
- [The Girl Next Door](/docs/confidants/fbi) provided me with a link to click, once. Doing so opened up Google Assistant - which NEVER should be possible.
- Finally, after our day in town, Google killed the USB functionality on my cell phone. The power still worked, but the USB function was dead. It had not been exposed to the elements.

No, they were telling me to replace the phone. 3 times. `# Verified.` 

So, I did. The hope was that this would throw off my followers long enough for me to visit the Night Owl tavern without interruption.

Checking my phone, I found the address of this location. I also found something unexpected; the tavern had been marked as "Closed Permanently." 

This was NOT the case 1 week ago.

Regardless, I was already here, so I was going to make an attempt. I made my way to the tavern without much trouble. I pulled in to the dead-end street next to it, and exited the car. At the end of the street, there was a running car, sitting idle, facing away from me. As I put on my coat, this car turned to face me - but did not leave.

My heart started to race. The FBI were watching me.

I walked briskly to the tavern, under cover of the falling snow, surveying the property for cameras. I did not see any.

As I found the front door, I noticed that the lock had been removed. There was an empty hole in the door, with light shining through it. There was still power inside.

I slipped 3 business cards into the hole, and walked back to my car. My heart was pounding at this point. There was blood pumping in my ears.

As I turned the car on, I began to pivot, turning around to exit the dead-end street. It was then that two more vehicles pulled onto the street in front of me, blocking my exit. 

I began to feel like I was being boxed-in. Like this was a set-up, or a sting operation. Like maybe these weren't FBI officials after all.

Regardless, I slipped through an open space between a parked car, and the two vehicles coming towards me. I made my way back onto the main road. I was totally freaking out at this point - but the vehicles did not attempt to follow me.

This was a warning: "What you did was reckless. That could have been a drug house."

Point. Taken.

Upon leaving, I found that the voice and data service on my new phone had stopped working. I was alone and lost, without a cell phone, in a foreign city after dark.

POINT. TAKEN.

Completely freaking-out, I stopped at the local Rite-Aid. There was a man conspicuously standing near the entrance, waiting for me. He wasn't an employee - and Rite Aids in Buffalo don't typically have security guards. He was there for me.

And he helped me out. He gave me directions back to the cell phone store at the Walden Galleria Mall. 

The FBI were helping me.

## CAT
---
```
data.stats.symptoms = [
    - fear
    - gratitude
]
```