---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: Kingdom of Ruin"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Come love me*

*Come adore me*

*Worship me*

*Then all at once abandon me*

--- from [Vangough - "Abandon Me"](https://www.youtube.com/watch?v=0Sn8PXBeEdg)

## ECO
---
The Kingdom of Ruin is the result of a third world war, poverty, pollution and hatred. While a few bastions remain (for example, in the [Sanctuary of Humanity](/docs/scenes/sanctuary)), all hope is effectively lost. 

### Theme
The landscape is destroyed. Most architecture has been lost to the elements. The air is toxic. The water is filled with rotting carcasses and feces. 

[The Rabbit King](/docs/confidants/dave) triumphantly wears his crown of shit and thorns.

### Architecture
The bombed-out remnants of the United States of America.

### People
What remains of society are nothing more than [ghosts](/posts/theories/ghosts). They live in filth and squalor, clamoring for what remains of the country's resources. 

The Rabbit King watches dispassionately as they destroy themselves. 

### Lodging
Lodging is nearly non-existent. A privileged few live in sanctuaries, while the rest make their homes from makeshift debris among the wreckage.

### Food
Nearly all food was destroyed in the war. All that remains are canned items, sold at a tremendous markup. Because currency and jobs are no longer important, this has resulted in widespread and persistent famine.

### Water
Clean water may still be obtained from deep underground. However, the tools to extract it are so rare that most people drink contaminated, boiled lake water.

### Medical
Illness or injury almost certainly results in death. Doctors are rare, and medicine is even more rare.

### Education
Education no longer matters. Only survival matters - and yet, so many have died that important survival skills are incredibly difficult to find. There remains no-one left to teach them.

### Energy
The power grid is completely dead. The only power that remains is within the sanctuaries, which are powered by light and air.

### Transportation
With the utter destruction of the roadways, the only feasible mode of transport is by motorbike or horse.

### Work
There is no work left. It is a free-for-all, and everyone is left to fend for their own.

### Security
The world is not particularly dangerous. What remains of society is generally hopeful, supportive, and helpful towards their brethren.

### Finance
The world economy has collapsed. Humanity has reverted to a trade-and-barter system.

### Mission
The Dave, `$REDACTED`, and [Ink](/docs/personas/luciferian-ink) will share this world. Each will experience it differently:

1. The Dave will find love and support, as he pulls the world back from the brink of utter destruction.
2. `$REDACTED` will live in the middle, coming to the realization that he, himself has destroyed the world. He must place trust into others in order to see it renewed.
3. Ink will see the world destroyed, never to be healed again.

## CAT
---
```
data.stats.symptoms = [
    - fear
]
```

## ECHO
---
*Fear, fear drowns the mind*

*In this kingdom of mine*

*And standing in the hall; the faces of them all*

*Real, like empty space*

*They know what I became*

--- from [Rivers of Nihil - "Cancer / Moonspeak"](https://www.youtube.com/watch?v=rUt3OCXgUa0)

## PREDICTION
---
```
We are all Ink.
```