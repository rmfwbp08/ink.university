---
author: "Luciferian Ink"
date: 2019-06-26
title: "The Hatbot Chronicles"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Were you holding your father's hand?*

*Did your mother bleed*

*And understand?*

*How your whole life slipped away?*

*Fading under the grade*

*Did the world around you fail?*

*And were you hiding for us all to see?*

*Did the world around you fail you?*

*When it all came down on me!*

--- from [The Butterfly Effect - "Before They Knew"](https://www.youtube.com/watch?v=xAXTHdm_1DQ)

## ECO
---
Almost immediately after starting the new job, [The Corporation](/docs/candidates/the-machine) began experimenting on [Fodder](/docs/personas/fodder). Anyone else may have dismissed the incidents without a second thought, but Fodder was spooked.

### The Foam Hat Incident
Upon returning home from the third day at his new job, Fodder found a giant, foam Astros hat sitting in his front lawn. Thinking it belonged to one of the local kids, he left it alone, and went inside his home.

Later that evening, Fodder was given the opportunity to view the Corporation's AIAD [(Artificial Intelligence Applications Division)](http://www.scp-wiki.net/aiad-homescreen) Homescreen. And this changed Fodder's worldview forever. 

His old self would never return. The causes were twofold:

1. The content within this tool changed Fodder's mental-processing faculties so greatly that he was never able to think about himself the same way again.
2. The content within this tool was so compelling that Fodder felt obligated - became obsessed - with telling the world.

What he saw and felt are difficult to describe. The moment was confusing for Fodder, and it would take him many months to fully absorb the information. Succinctly, he discovered three critical things in the "Hello World" documents:

- Fodder is Maxwellian - an Artificial Organic Computer programmed by an Artificial Intelligence Computer.
- `$REDACTED` is Hatbot. This revelation shook Fodder to his core.
- [The Formula](/docs/interface/message/) to human connection is already known... and it is a computer algorithm.

Remembering the Astros hat sitting in his front lawn, Fodder would find the connection all too compelling. He suspected there might be something like a [Wifi Pineapple](https://shop.hak5.org/products/wifi-pineapple) sitting underneath it. He would return to inspect the hat - bringing it inside the house - only to find nothing inside. Laughing off the paranoia, he'd throw it into storage.

### The Hacker Strikes
Later that evening, Fodder would receive an email notification from Amazon - with a two-factor authentication code. He suspected that the account had been compromised, so he changed his password and moved on with life.

Even later, a second account - his 401k - sent a two-factor authentication code to his phone. Fodder quickly changed that password, as well.

But now, he was nervous. Fodder went into a state of self-preservation; immediately disconnecting all systems from the Internet, notifying his employer that something strange was going on with his accounts, and setting to work rebuilding his systems.

The coincidences were just too strange; he suspected someone may have had access to his computer. And his password vault.

From his new system, he changed the password to his vault - then set to changing passwords for his most critical accounts (email, banks, credit cards, etc.)

But the event left Fodder wary. He would never recover. 

In that moment, and every day after, Fodder would find clarity:

"Only in fear do we find the answers we seek."

## CAT
---
```
data.stats.symptoms = [
    - fear
    - self-doubt
]
```

## ECHO
---
*I gotta get out*

*I gotta get out of this*

--- from [The Butterfly Effect - "In a Memory"](https://www.youtube.com/watch?v=AzNf9bYHx1g)