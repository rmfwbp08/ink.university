---
author: "Luciferian Ink"
date: 2019-12-03
title: "Synchronicities"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## ECO
---
This day started bright and early. I rented an SUV, and made my way to Buffalo - where I would ghost you for the third and final time. There was a specific tavern that I needed to visit.

Along the way, I would catch-up on the Harmontown podcast. I was several episodes behind, and I wanted to know if they'd been talking about me.

Indeed, it sure seemed like they were.

At one point, [Dan](/docs/confidants/hype-man) cracked a joke, "Why would anyone name their restaurant BJ's?" The crowd roared in laughter.

Not 5 seconds later, a car pulled in front of me - with the bumper-sticker, "I <3 BJ's Restaurant!"

Spooky.

At a second point, Dan talked about a friend of his, Mike, passing away. I immediately connected this with [Davos](/docs/confidants/davos). Dan went on to describe how much he respected this man, who had spent his entire life's work doing this one, special thing. He said that Mike was "chosen from birth" to complete this task.

His crew would push for clarification, but Dan would move on.

We believe that [Ink](/docs/personas/luciferian-ink) has officially replaced Davos.

## CAT
---
```
data.stats.symptoms = [
    - wonder
    - confidence
]
```