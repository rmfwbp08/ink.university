---
author: "Luciferian Ink"
date: 2001-11-01
title: "The Invisible Child"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A message from [The Historian](/docs/confidants/historian).

## ECO
---
In 8th grade, the class would travel to Washington D.C. on vacation. [Father](/docs/confidants/father) would chaperone the trip - keeping watch over Fodder the entire time.

To Fodder's humiliation, Father would see everything; he would see Fodder's terrible group of friends, he would see that Fodder was a loser, he would see that Fodder never really interacted with anyone.

But he would do nothing. He would say nothing. He would never acknowledge that Fodder was anything other than a normal child.

He would never [reflect Fodder back at Fodder](/posts/journal/1995.11.22.0/).

Thus, Fodder would never learn to change.

Because Father never changed.

## CAT
---
```
data.stats.symptoms = [
    - humiliation
    - loneliness
]
```

## ECHO
---
*If a DC-10 ever fell on your head and you're*

*Laying in the ground all messy and dead*

*Or a Mack truck ran over you*

*Or you suddenly die in your Sunday pew*

*Do you know where you're gonna go?*

--- from [Audio Adrenaline - "DC-10"](https://www.youtube.com/watch?v=ysV7sY2OMEU)