---
author: "Luciferian Ink"
date: 2024-11-01
publishdate: 2020-05-24
title: "The Tempest"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*They are sick, they are poor*

*And they die by the thousands*

*As we look away*

*They are wolves at the door*

*And they're not gonna move us or get in our way*

*'Cause we don't have the time*

*Here at the top of the world*

--- from [Thrice - "Cold Cash and Colder Hearts"](https://www.youtube.com/watch?v=ikUrxI7g6BM)

## ECO
---
The above band's name is "Thrice", fitting cleanly into [Fodder's Theory of Three](/posts/journal/2024.11.02.0/).

The shape of The Tempest is comprised from multiples of 3: 666-1.

[![The Shape of The Tempest](/static/images/singularity.white.png)](/static/images/singularity.white.png)

[The All-Mother](/docs/confidants/mother) represents 1, located at the center/front of the Tempest.

The circle/all other life is subordinate, following in the All-Mother's steps. The closer to the eye of the storm, the more chaotic an entity's quality of life.

In this particular arrangement, The Tempest is so naturally violent that its energy must be saved for only the truest of followers. Those who will not conform should be left to their suffering.

## CAT
---
```
data.stats.symptoms = [
    - certainty
]
```

## ECHO
---
*The sea, the storm was always you*

*The call of the cascade*

*I see no god in here but you*

*The water, the cascade*

*And we could drown the world in truth, in concord*

*So dare I just let them set their fires?*

*They'll find no witness*

*I'll swear by no saints*

--- from [Caligula's Horse - "The Tempest"](https://www.youtube.com/watch?v=dNc5PT645sU)