---
author: "Luciferian Ink"
title: "Reviewer Rank"
weight: 10
categories: "metric"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A communication from [The Girl Next Door](/docs/confidants/fbi).

## ECO
---
The Reviewer rank is a 1-5 star rating system. It is used to determine the reliability of a confidant's information.

For example:

```
1: The reviewer is probably not aware of us at all. Their message was not originally intended for us.
2: The reviewer sometimes touches on topics that are relevant to us. Involvement is questionable. Coincidence is likely.
3: The reviewer has sent messages that - while not conclusive proof - suggest involvement with our plans.
4: The reviewer has a solid history of providing relevant, useful feedback. Hard evidence is still missing.
5: The reviewer has proven their loyalty by exposing themselves to us.
```