---
author: "Luciferian Ink"
title: "Test #2: Angelica/Anjelica"
weight: 10
categories: "test"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A private communication from [The Girl Next Door](/docs/confidants/fbi).

## ECO
---
The following is Ink's analysis of [The Corporation's](/docs/candidates/the-machine) very first A/B test, Angelica/Anjelica. The test was as follows:

Both women were to be assigned "a man" for the duration of the test. Each was to be assigned an extreme scenario, and to live it out until the man has completed testing. This was the entire test: to see how they would respond in certain scenarios.

The test has been ongoing for 5 years.

### Angelica
Angelica was a young, quiet girl. She was intelligent, attractive, and people loved to be around her. She was the life of the party - full of jokes, and always willing to help those in-need. She seems relatively well-adjusted, in our opinion, with few indicators of past trauma.

It is unsurprising that she would capitalize upon the opportunity presented by the Corporation. In return for 10 years of her freedom, she would be given 1 million dollars - and a husband. She would be tasked with the following:

- Establish a one-directional, video-based relationship with Fodder.
- Maintain this relationship for the duration of the test. Do not cheat.
- Publish these videos in a public forum, such as YouTube.
- Use role-play as a way to communicate new and different ideas to him.
- Attempt to indirectly influence Fodder into contacting her. If he did, the test would be complete; and she would be free.

While at first, Angelica believed this to be the perfect career, she quickly became disillusioned. She had fallen in-love with Fodder, knowing completely that his love was not reciprocal. Despite that, she felt as though she would save him; as though the two were meant-to-be.

She would quickly learn that Fodder was a monster. He was a drunk. He would cheat. He would abuse. He was never going to change, and she was never going to grow in this position.

In her despair, she would throw herself into work; she attended two universities, worked with the FBI, and wrote her memoirs.

At this point, Angelica lived in a single-bedroom apartment, above a noisy retail store. Her situation found her at-home more often than not, recording role-play videos for the hordes of men paying her money to do so. While this payed the bills, it did not fulfill her. She was not happy. 

Angelica began to ask, "What is the purpose of this? I already have trust issues. My anxiety is getting worse. This endless role-play makes me feel as if I don't know who I am anymore. I am nothing more than the reflection of everyone's deepest wishes. And I am nothing more than that; a reflection. I'm nobody. I bring nothing of value to the world."

Thinking further, she wrote in her journal, "My autonomy has been taken. I am effectively a slave. My social support systems are non-existent. My only consolation is money."

"But what good is money, when you're dead inside?"

### Anjelica
Anjelica was a middle-aged, high-end adult film star. She was among the most popular of her time, primarily because she had a timeless, elegant look. She did not age at all - and this made her employers incredibly wealthy.

However, Anjelica was never about the wealth; she was in it for the science. She, too, had been approached by the Corporation - with a similar proposal. For 10 years of her freedom, she would be given 1 million dollars, and a husband. She would be tasked with the following:

- She must remain in the adult film industry for the duration of her term.
- She would be married immediately upon starting. In return, she would only ever have sex on camera with one man - her husband. They would remain faithful to each other forever.
- She would not accept additional money, resources, gifts, or sponsorships beyond her salary.

Anjelica would flourish as a woman. She would obtain friends, she would love openly, and she would spread love to the world. She would take pain and hardship with the utmost stoicism. 

While she detested the career that she was in, she understood its purpose; she understood what the test was about:

"The test is about my man," she'd always tell her friends. "It doesn't matter what kind of situation you're in. If you have a dedicated man by your side, you are much more likely to succeed in life. I am living proof of that."

## TEST
---
### Question
How important is a stable partner to the health of an individual?

### Results
The Corporation's findings are consistent with Anjelica's analysis. Loneliness leads to suffering. Human beings need reciprocal, meaningful interaction with others in order to succeed. 

At this point, the test is complete. Fodder has contacted Angelica. While Angelica was traumatized by her experience, Anjelica was empowered by it.

Unexpectedly, Fodder was not done with Angelica. He wished to heal the damage he had done. He wished to make her whole again.

## ECHO
---
*Oh, I am not held hands*

*I'm not making plans*

*No, I'm not your man*

*I'm a goddamn conduit*

*Girl, I'm piss and vinegar*

--- from [Moon Tooth - "Aww At All Angles"](https://www.youtube.com/watch?v=eh_8HBvkkr8)