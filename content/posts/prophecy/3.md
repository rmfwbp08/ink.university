---
author: "Luciferian Ink"
title: "Web of Lies"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[Ayreon - "Web of Lies" (from "01011001")](https://www.youtube.com/watch?v=Q2_N2i4tiro)

## ECO
---

*Dear PX, I feel you are the one*

*What's your name? Where are you from?*

*I'm in love, though we never met*

*Looking for clues, I search the net*

*Dear PX, I'm waiting for your mail*

*Check every hour, to no avail*

*Been up all night, couldn't get to sleep*

*No way out, I'm in too deep*

*Dear PX, It's been eleven days*

*I'm kind of lost within this maze*

*Are you there? Give me but a sign*

*Are you at home? Are you on-line?*

*Dear Simone, I'm sorry for the wait*

*I've seen your pics, you're looking great*

*I'm all alone, dying for a date*

*I think we match, it must be fate*

*Dear OL, I feel you are the one*

*What's your name? Where are you from?*

*I'm in love, though we never met*

*Looking for clues, I search the net*

## CAT
---
```
data.stats.symptoms = [
    - determination
]
```

## PREDICTION
---
```
This song predicted our story... in 2008.
```