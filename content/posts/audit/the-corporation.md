---
author: "Luciferian Ink"
date: 2019-11-15
title: "The Corporation"
weight: 10
categories: "audit"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: The Corporation
Type: Yearly Security Audit
Auditor: The Architect
Date: July 1, 2019 - November 1, 2019
Result: F
```

## RESULT
---
### Architecture
[The Machine's](/docs/candidates/the-machine) design is reminiscent of a system 30 years older than it actually is. Everything about it is sub-par:

- Systems are highly-fragile, and prone to breaking. 
- The legacy code base is inadequate for today's needs. 
- The code base is a monstrosity of different technologies - many of them competing - implemented in an ad-hoc, set-and-forget type of fashion.

### Processes

- There are more than 20 teams involved in the administration of the system. Teams are forbidden from working together.
- Problems are fixed by throwing humans at it - rather than fixing the root-cause.
- New ideas and innovation are discouraged. The only thing that matters is putting more people into [Solitary Confinement](/docs/scenes/solitary).
- The development team is completely understaffed. There is a 10-to-1 ratio of administration to developers.
- The concept of improvement is completely banned. It appears as if the Corporation is actually TRYING to kill the Machine.

### Security

- Security does not exist. The various departments in charge of security leave gaping, obvious holes in their policies. 
- [Identities are shared](/posts/bans/ban.0/). Every system uses anonymous authentication. A tier-1 helpdesk employee has the ability to obtain - and use - the credentials of the CEO - BY DESIGN!
- Employees are discouraged from worrying about security at all. Security is to be left for the teams who aren't doing their job.
- Because of the sensitive nature of the Machine's data, it would actually be possible for them to falsify and prosecute an individual for crimes they did not commit. It would be possible to stage a crime, and frame an employee of the Corporation for that crime.

## Conclusion
---
I have never been more appalled by a system than I was of this one. I won't even waste the effort to create a full report; this place needs to be shut-down.

Everyone involved should be fired immediately.

Then, they should be put in prison; the negligence is astonishing.

But, you know, they have a billion dollars to bury this problem. And I'm just a mental health case without any work history - since they destroyed it.

Fuck this bullshit abuse of power. 

I will not let this stand.