---
author: "Luciferian Ink"
date: 2019-12-01
publishdate: 2019-11-30
title: "Voice"
weight: 10
categories: "ban"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[Ink's](/docs/personas/luciferian-ink) words fall on deaf ears.

## ECO
---
Employees of [The Corporation](/docs/candidates/the-machine) are forever and completely banned from communication with the outside world. This is to include:

- No contact with friends, family, or acquaintances.
- No contact with anyone who may disclose your whereabouts to the aforementioned.

Exceptions:

- Employees may speak and work openly among themselves. There are no restrictions on the distribution of information.
- By using the process of [trust and verification](/posts/theories/verification/), one is permitted to communicate with the outside world. (i.e. communication should be indirect, and through a third-party and/or artificial identity)

## CAT
---
```
data.stats.symptoms [
    - certainty
    - trust
]
```