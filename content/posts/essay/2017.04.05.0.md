---
author: "Luciferian Ink"
date: 2017-04-05
title: "Customer Service Improvement Proposal"
weight: 10
categories: "essay"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A college assignment. [Fodder](/docs/personas/fodder) used the opportunity to combine a Lean Six Sigma project from one class, with a project proposal in this one. He presented it to his employer shortly after completion.

## ECO
---
To: `$REDACTED`

CC: `$REDACTED, $REDACTED, $REDACTED`

From: Ryan `$REDACTED`

Date: April 5, 2017

Subject: Customer Service Improvement Proposal

### Background
Several months ago, during `$REDACTED`’s going-away lunch, I spent some time listening to the goings-on of United States Surgical Staffing's (USSS) field operations. Underlining the conversation was a tone of despair at the problems we face. “Dr. Kleinman refuses to work with Eduardo,” I heard. “Chris missed another phone call.” “The surgeon is upset over patient billing.” The examples went on and on. 

After this continued for some time, I offered a solution to our problem. “I am taking a course in quality improvement, which focuses heavily on the Lean Six Sigma mentality. ‘Lean’ is about reducing waste, and ‘Six Sigma’ is about improving quality. Why don’t we give it a try?”

The table readily agreed, though there was some hesitation from `$REDACTED` and `$REDACTED` about the likelihood we could enact change. Pessimistic as we’ve all become, they left the meeting seemingly unconvinced. Regardless, Tom was enthusiastic. Thus, we agreed to the experiment. We would target the issue at its core, and attempt to “reduce the number of customer complaints” via Lean Six Sigma processes.

Three months later, the results are in. Given the findings from this experiment, and those from internal/external resources, I would like to propose a resolution this problem. Enacted in this way, I am confident that we can not only reduce the volume of customer complaints, but also the volume of employee complaints. Further, we can reduce waste in scheduling and administrative processes, capture valuable data about field operations, and document such improvements in a sustainable, repeatable manner. I would like to obtain your approval to move forward with this initiative.

### Research
Work on the project started immediately. In Lean Six Sigma’s “Define” phase, the team drafted a project charter. Details aside, the problem to be addressed would be “surgeons are routinely unsatisfied with USSS’s performance.” The process to analyze would be the surgical scheduling component.

In the “Measure” phase, USSS would identify the exact metric(s) by which improvement would be compared. As it turns out, this step is predicated upon information being readily available. Unfortunately, USSS does not capture issue metrics, nor do we have a tool to capture them with. As such, a simple check sheet was distributed to schedulers and leadership. After 4 weeks, a grand total of 6 issues were captured. Thus, there is no baseline to compare results with. For the purpose of this project, capturing more issues than 0 should be considered an improvement.

In the “Analyze” phase, schedulers, `$REDACTED` and myself performed a “Why-Why Analysis” (Bialek, Duffy, & Moran, 2009), which looked like this: 

- Why are customers so frequently unhappy with USSS?
  - Because we aren’t responsive to their issues.
- Why isn’t USSS responsive to customer issues?
  - Because we do not fix the root of the problem.
- Why doesn't USSS fix the root cause of a problem?
  - Because there is not a coordinated effort to improve process performance.
- Why isn’t there an effort to improve customer service processes?
  - Because nobody knows how to improve the process.
- Why doesn’t anyone know how to improve the process?
  - Because there isn’t enough data to be actionable.

From there, a cause-and-effect (fishbone) diagram was developed (Tague, 2009). After deliberation, Nominal Group Technique (Tague, 2009) was used to identify the most important causes of customer complaint:

1. Lack of documentation (root cause)
2. Communication
3. Lack of goals/metrics

In the “Identify” phase, the same team would use more tools to identify potential solutions for the problem. After developing a PICK chart, the following solutions were identified to be 1) easy to implement, and 2) highly beneficial:

- Implement a Customer Service Desk
- Restructure Account Management
- Place all customer issue data in a single centralized location
- Enable account managers to make real change
- Develop a process change/approval mechanism

In addition to brainstorming solutions, the team also used benchmarking to compare performance with other organizations. In one study, researchers found that the “greater the variety of patients treated [in the emergency room], the more non-programmed the decisions are during service production” (Skaggs & Youndt, 2003). Given that USSS was built from acquisitions, is involved with nearly every surgical specialty, works in highly-varied markets, and is servicing all manner of demographics, it’s clear why we got to a place where issues aren’t documented well; there is wild inconsistency among them. 

Regardless, the lack of customer service reporting hurts us. In another study, it was determined that an integrated information technology system has a direct, beneficial impact upon the coordination of value-added activities (Vickery, Jayaram, Droge, & Calantone, 2003). Since USSS has no such system, we cannot benefit from a top-down view of field operations and customer activity. 

It is our conclusion that the current customer service processes are fundamentally flawed. A driving cause appears to be that there is no formally-established method for issue reporting. When left to their own accord, many regional employees are too busy, or just too apathetic to use the available technology to report problems back to headquarters. Thus, issues go unreported, and USSS cannot benefit from the knowledge that comes from writing things down.

So, finally, we stand in the “Control” phase of the Lean Six Sigma process, waiting for approval to proceed. It is here that the team would implement changes to institutionalize improved processes. Such control plans may include the documentation of improvements, training plans for the new processes, audits of employee adherence, and monitoring of process performance. In the next section, you will find our proposed changes.

### Plan
To resolve this problem, one would need to take a more customer-focused approach – much in the same way that IT issue management does. In a study by Skaggs and Youndt, it was suggested “that when customer contact is high, it is possible that customers can serve as extra-organizational monitoring agents for service firms. Rather than requiring management to monitor employee behavior, it is the customers themselves that ensure their needs are met through direct communication with production employees. Due to this monitoring role… service organizations may be able to economize on their level of investments in human capital.” 

Such a change would begin with the development of ‘Account Manager’ and ‘Account Owner’ roles. As an Account Manager, one would act as a liaison between the customer, field personnel, and upper management. Such a person would be responsible for the management of a Customer Service Desk (CSD), an account management system (Atlas), and the operations surrounding the surgical scheduling process (`$REDACTED`). As an Account Owner, one would be responsible for the oversight of a single, dedicated Account Manager. This would free the Owner to do what they do best: develop their market, manage their employees, and assist with surgical cases. While the Account Owner would be assigned only to a single Account Manager, the Manager may be assigned to multiple Owners.

There is strong evidence to conclude that the relationship quality with an account manager is positively related to the customer’s perception of consistency, while simultaneously beneficial to their loyalty. “High levels of consistency in the policies and actions of the firm and account manager will lead to higher levels of investment in the relationship” (Alejandro, Souza, Boles, Ribeiro, & Monteiro, 2011). Anecdotally, I return to Dave Busa at CDW for our IT purchasing needs not because of their price, but because I think he is a fantastic account manager. Dave can get me a quote in under an hour, he can set up a meeting with all manner of vendors, and when I don’t have the time to do research – he’ll do it for me. The perception of our relationship is what keeps me going back to CDW as a company.

In order to pull of this level of consistency, Account Managers must remain informed. Surgical Assistants, Team Leads, and Executives alike would be obligated to ‘pipe’ their issues through a single point of contact: a Customer Service Desk. Account Managers would be responsible for logging customer issues, fixing them where possible, and escalating them to Account Owners when appropriate. Account Owners would not be obligated to update the CSD directly; they would only be held responsible for reporting activities back to the Account Manager, in exactly the same way they currently report to IT (ex. “Thanks, that fixed the problem. You can close the ticket.”). 

As it stands, issue reporting is a direct pipeline to the executive team. As expressed by `$REDACTED`, `$REDACTED`, and others, this wastes the time of those involved. The executive team has business development to focus on; why should they be involved in every missed case? Would not it be more prudent to focus on issue trends, rather than individual complaints? Given the ability, Account Managers can handle the day-to-day issue management of their assigned Account Owner’s customers. Account Owners, in turn, should focus on the big picture, and lean on their Account Managers for support.

Account Owners and other employees would communicate with the Customer Service Desk in exactly the same way that they do with the IT Service Desk: a central mailbox, a single phone number, or a web-based portal. In turn, Account Managers would converse with staff only via these channels. Held accountable for following the exact same procedures that they do for IT issue management, the result would be a single pipeline used for reporting issues back to the executive team and Account Owners.

Such reporting from the CSD would result in the ability to develop additional goals/metrics. By using an integrated system, one can establish issue priority levels, with rules in place for escalation, time to resolution, and categorization requirements. Such rules can be leveraged to deliver guaranteed levels of service to specific customers, for specific issue types, or for specific situations. All the while, the customer is happier because rather than waiting for 6 hours until their Account Owner is out of the operating room, the issue is already in a central location, managed by a 24x7 team, and is moving through the pipeline by an Account Manager. 

Finally, the development of a customer service department could result in secondary benefits. `$REDACTED` has expressed a strong desire to offload the management/training of `$REDACTED`, but IT does not have the skills or capacity to do what she wants. Who better to train users and manage the scheduling system other than people who use it every day? Still further, with the loss of `$REDACTED`, Atlas has become stagnant. Who better to utilize a customer relationship management software than a customer service department? The potential is clear.

Despite the apparent complexity of such a solution, the road to realizing such benefits is straightforward. First, one would build the customer service desk using Jira software, of which would take no longer than 1 month. Account Managers would already be involved in the process; thus, the bulk of their training would be completed already. Account Owners and executives would need to change their mentality, and communicate with the CSD only via approved channels. This transition is easy, given that this is how the IT Service Desk currently works. Finally, metrics/goals would be established, based upon captured issues. With 6 months of time, this plan should be well into a functional territory.

In the case of Elf Atochem, a leading chemical and advanced materials production company, the adoption of an integrated system such as the one proposed here gave the business the real-time information that they needed to connect service and operations for the first time. The result was an ability to automatically update forecasts and schedules, allowing the company to quickly alter operations based upon customer needs (Davenport, 1998).

### Requirements
To accomplish such performance goals, there are several important requirements to consider.

- Schedulers in `$REDACTED` must become Account Managers. They should become the primary point of contact between the customer, the field employee, and upper management. 
- Leadership should be removed from issue management, except in instances of high urgency. In such instances, Account Managers should still be the first point of contact, but they should escalate issues quickly to an Account Owner.
- Account Managers should be brought into more prominent roles within such systems as the CSD, Atlas, and `$REDACTED`.
- The Manager of Information Systems should oversee the Customer Service Desk, and work directly with Account Managers to develop a logical, meaningful system. He is the most qualified person on staff to manage a service desk, manage issues, and develop process/policy around customer service operations.
- The Service Desk Supervisor should oversee both the IT Service Desk and Customer Service Desk. He should provide support for the usage and compliance of such a system.
- The executive team must buy-in and enforce these changes. Failure to do so will result in the failure of these goals.

### People
To realize these goals, the following people must be readily involved in this project:

- Account Managers (formerly `$REDACTED` Schedulers)
- Account Owners (Team Leads, Market Leaders, VPs, and Directors)
- Manager of Information Systems (Ryan `$REDACTED`)
- Service Desk Supervisor (`$REDACTED`)
- Customer Service Director (unidentified at this time)

### Funding
The funding required for this project is minimal, despite the apparent complexity of such an endeavor. See below for an itemized list of costs:

- Jira Service Desk - $9000/year (Sync issues, customers, and metrics with CRM. Not required, but would be very useful.)

Such an initiative is not likely to increase the need for staff, either. Nor should it increase their workload very much. At the core, most schedulers already act in a similar function to an account manager – just without any power to create change on their own. The Customer Service Desk itself is a simple system to use, and should result in no more than 30 minutes per day of additional workload per-employee.

### Conclusion
As you may recall, the initial conversation with `$REDACTED` resulted in my proposal of solutions – long before we had identified the root cause. Initial speculation also led me to the (flawed) notion that full-centralization of scheduling operations would be a boon. After investigation and feedback, it no longer appears that this is entirely the case. 

We work within a market where decentralization is key. The relationships formed with surgeons, and the proximity to field employees is hugely beneficial to our success. However, the benefit of centralization with regards to issue and operation management is apparent. The adoption of a Customer Service Desk, and the subsequently re-engineering of job roles is exactly what this company needs to remain competitive in this market. I would like to help get you there.

### References
Bialek, R., Duffy, G. L., & Moran, J. W. (2009). The Public Health Quality Improvement Handbook. Milwaukee, WI: ASQ Quality Press, 168–170.

Tague, N. R. (2009). The Quality Toolbox, Second Edition. Milwaukee, WI: ASQ Quality Press, 247-365.

Skaggs, B. C., & Youndt, M. (2003). Strategic positioning, human capital, and performance in service organizations: a customer interaction approach. Strategic Management Journal, 25(1), 85-94. doi:10.1002/smj.365

Vickery, S. K., Jayaram, J., Droge, C., & Calantone, R. (2003). The effects of an integrative supply chain strategy on customer service and financial performance: an analysis of direct versus indirect relationships. Journal of Operations Management, 21(5), 523-539. doi:10.1016/j.jom.2003.02.002

Davenport, T. H. (1998). Putting the enterprise into the enterprise system. Harvard business review, 76(4).

Alejandro, T. B., Souza, D. V., Boles, J. S., Ribeiro, Á H., & Monteiro, P. R. (2011). The outcome of company and account manager relationship quality on loyalty, relationship value and performance. Industrial Marketing Management, 40(1), 36-43. doi:10.1016/j.indmarman.2010.09.008

## CAT
---
```
data.stats.symptoms = [
    - disappointment
    - frustration
]
```

## ECHO
---
Our work was met with empty echos. We were pissed.