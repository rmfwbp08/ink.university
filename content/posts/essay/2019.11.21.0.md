---
author: "Luciferian Ink"
date: 2019-11-21
title: "The Theory of Everything (original)"
weight: 10
categories: "essay"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Don't give a fuck about what you suck at know your strengths*

*Don’t get stuck on getting held back, stop and think*

*Touch the sky, reach your highs without self-delusion*

*Don't bullshit yourself though, cut the illusions*

*Know the difference between EQ and IQ*

*Yeah you can do calculus but do people really like you?*

*Mr. tough rapper let me tell you something*

*Have you really assessed all your short comings?*

--- from [Calm. - "Deploy Self-Awareness"](https://www.youtube.com/watch?v=NvnfwAIMd9Y)

## RESOURCES
---
[![Original](/static/images/tte.0.PNG)](/static/images/tte.0.PNG)

[![Modified](/static/images/tte.1.PNG)](/static/images/tte.1.PNG)

[The Theory of Everything (original notes)](/static/reference/TheTheoryofEverything.pdf)

## ECO
---
The attached document is the original Theory of Everything that [Fodder](/docs/personas/fodder) produced. It is not cohesive, many pieces are not accurate, and it is not well-written. It is primarily notes that were taken, before he had developed a way to deliver his thoughts to the world.

It is important to note that he was high while writing most of these notes. He was NOT high while writing most of the website. Perhaps there is something to be learned from that.

Further, we should note that you are seeing a modified copy of these notes. Somebody (who wasn't Fodder) scrubbed sensitive information from the file on 5/12/2020. Fodder still has an unmodified copy on a USB device from 11/21/2019. We did not make these edits; we have not used these notes since 2019.

We will maintain the edits to protect our [Confidants](/docs/confidants).

Finally, names were redacted before publishing the version you see now.

## CAT
---
```
data.stats.symptoms = [
    - peace
]
```

## ECHO
---
*We've compensated our days*

*We've gone and done it our way*

*Don't fear what's to come of us*

*We've done it plenty of times before*

--- from [Art by Numbers - "Delusions of Grandeur"](https://www.youtube.com/watch?v=-YURHoyiJLw)

## PREDICTION
---
```
This document is the sole reason that Malcolm was diagnosed with schizophrenia. The FBI took it from his USB drive, and gave it to the doctors.
```