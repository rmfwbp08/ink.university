---
author: "Luciferian Ink"
date: 2007-11-29
title: "Turn it Up, My Ears are Barely Even Bleeding"
weight: 10
categories: "essay"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A college assignment.

## ECO
---

It’s 6:45 in the morning. I’m tired. I don’t want to go to college. And the last thing I want to hear is loud music. Yet that is precisely what I have to put up with for the hour I am on the bus in the mornings. Sixty minutes of listening to music about “smacking that” from the guy next to me’s MP3 player is enough to drive anyone crazy.
Maybe I am a little biased against that particular genre of music, but the fact remains that most people listen to their music too loud, and too often. "Over the last twenty years, environmental noise has doubled each decade," says Marshall Chasin, director of auditory research at the Musicians' Clinic of Canada (“Music” Par. 3). Nearly every male I went to high school with had an expensive stereo system in their cars, complete with overbearing subwoofers. Listening much too loudly to their MP3 players in public is a trait common to the people who have them; so much so that the lyrics are audible to those around. Twenty-two million American adults own an iPod or other digital-music player, and studies show that sustained listening, even at moderate volume, can cause serious harm (“Music” Par. 3). Impressing those around them is all many of these people think about, instead of the people they are harming. While loud music not only disturbs others, it also causes irreversible damage to those listening to it.

In fact, most car audio loudspeakers hover in the 90 decibel range, but can be much louder when doubled with amplifiers and subwoofers. (“Car” sec. 4”) The human ear begins to receive damage at 100 decibels in merely a half hour. (Knox par. 11) Earbuds can actually be much more damaging, as the noise is amplified by the proximity to the ear canal. Thankfully, noise deadens within a short enough range as to cause minimal damage to the people around, but think of the person inside the vehicle, or behind the earbuds. Placing the source so closely to the ear, it only takes 85 decibels to begin to damage the ear. Many MP3 players can reach levels of 120 decibels. (New) That is loud enough to the words from across a quiet room. Or the bad words at least.

Indeed, this is why families with children undoubtedly don’t want to expose their children to the language and morals that define the genres of music that are most often listened to the loudest. Blaring music doesn’t come from people who listen to classical. Often you will hear the monotonous rhythms of the rap genre, with their descriptions of life in the hood and vulgar language. You may hear the screams of rock music; loud and seemingly unintelligible, often thought of as Satanic by the older generation. Smoking and drinking is a common theme in country music. Rick Warren writes in The Purpose Driven Church, "I reject the idea that music styles can be judged as either ‘good’ or ‘bad’...no particular style of music is ‘sacred’...There is no such thing as ‘Christian music’, only Christian lyrics." (Warren) All of these things can be offensive to those who do not care for it, especially the elderly, who grew up with a completely different style of music. Small children can be affected also. What a little kid defines as “cool” is determined almost entirely by those around them; thus a bad influence will often result in a bad kid. People nowadays just don’t seem to care about the people they are affecting by their music, including themselves.

Why then, is loud music so culturally accepted as it is? The fact is that it is only accepted among certain groups, such as the youth. Speakers systems within cars are becoming a competition as to who can have the biggest and the best. Pushing their limits, kids are willing to sacrifice their hearing to impress those around them. MP3 players have become so commonplace that, much like the cell phone, people abuse them in public. Because so many people around the abusers do not care, or are even impressed, they do not take into consideration the effects that their abuse may have on others. Just like you wouldn’t force your religion upon someone, you would not force your style of music. Would it make sense for a pizza delivery guy to pull up to a house with his music on full blast? Surely, there is a chance someone may be offended, or just annoyed. This person’s wage depends on impressing others in a different way, therefore they adapt. Why can’t the abusers of their music learn to adapt, when so many people are upset by their actions.

Therefore, it seems imperative that people should be conscious of the effects that their music may have on those around them. Not only should they pay attention to the music, but also to their actions, as one never knows how he may affect the up-and-coming generation.

### Works Cited

“Car Audio Speakers Overview” Car Audio Book.com. 2007: 36 Pars.	<http://www.caraudiobook.com>

Knox, Richard. “Kids' Use of Earbuds Worries Hearing Experts.”. National Public Radio.  26 Apr. 2007: 34 Pars. <http://www.npr.org>

“Music Making Fans Deaf?” Rolling Stone. 18 Nov. 2005: 18 Pars. <http://www.rollingstone.com>

“New Decibel-Limiting Earbuds Could Reduce Hearing Damage Caused By iPods” American 
	Hearing Research Foundation. 2004: 4 Pars. <http://www.american-hearing.org> 
    
Warren, Rick. “The Purpose Driven Church”  Zondervan. 1995: 408 Pages.