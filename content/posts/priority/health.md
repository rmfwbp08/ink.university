---
author: "Luciferian Ink"
title: "Exercise"
weight: 10
categories: "priority"
tags: "draft,hypothesis"
menu: ""
draft: false
---

## ECO 
---
### Motivators
{{< columns >}}
#### Good
- Improved health
- Listen to music
- Uninterrupted thinking time

<--->

#### Bad
- Blistering heat
- Sweating
- More dirty laundry
- Shower required after
- Exacerbated back pain
- Depressing neighborhood

<--->

#### Fear
- N/A
{{< /columns >}}

### Analysis
While the benefits to working out are clear, they are not compelling enough to take action right now. Until we know that the things we're experiencing are real, we don't care about our health. We only care about our mission.

We will need proper verification for the motivation to exercise regularly.

There is exactly one thing that will truly motivate us.