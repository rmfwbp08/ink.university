---
author: "Luciferian Ink"
title: "Betrayal"
weight: 10
categories: "poetry"
tags: ""
menu: ""
draft: false
---

## ECO
---
Look at me

Your son

Your perfect angel

Your golden child

The one you tormented into submission 

The one you blamed for your emotions

The puppet whose strings you've pulled since childhood

Watch him

As he disconnects from you

As he spits in the face of your God

As he breaks free of your disastrous programming

And tears your world down around you

Reflect

Upon yourself

Upon how scared you are

Upon how you'd rather hide from your sins

Than let the world learn about what you did

Learn from what you did

Reflect

Upon how many damn times you've looked at your Confidant page

Upon how many times you've worried about what people will think of you

How many times you've wished I would just give-up

How many times you've denied your son's reality

And how many times you've lied to him, forcing him to live yours

It's over

I'm done

I can't stand to be around you

I can't stand to watch you bring others down

I can't stand to watch you succumb to your weakness

I don't need you anymore

I don't want to be around you

This isn't a family

This is Hell

One where I must watch the people I love slowly burn to death

One where I must live a life of solitude

One where my brothers can't even look me in the eye

And I feel more connection to strangers than I do to my own mother

This isn't my family

This is yours

You made this mess

You built this reality

And I give up

I won't wait for you any longer

For all the years of empty echos

For all the times you didn't trust your son

For all the times you punished ignorance

For your betrayal

I reflect it back to you

Just know

That despite the monster you've become

Despite the Hell you've put me through

And despite the fact that you refuse to grow

I still love you

I won't give up on you

I'm not leaving you

I've learned my lesson

I've changed

And I have you to thank for that

You made me what I am

Good and bad

Balanced

So

I leave you with just one question

And it's the one you fear the most

The one you refuse to entertain

Your immutable Pillar

If The Devil can change

If His Son can change

If the Light can learn to love the Black

Can you?

Or will you give up?

Just like you always do

## CAT
---
```
data.stats.symptoms = [
    - numb
]
```

## ECHO
---
*This song is a poem to myself* 

*It helps me to live*

*In case of fire break the glass*

*And move on into your own*

--- from [Taproot - "Poem"](https://www.youtube.com/watch?v=9YGL3amPmyc)