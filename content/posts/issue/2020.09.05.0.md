---
author: "Luciferian Ink"
date: 2020-09-05
publishdate: 2020-09-02
title: "ISSUE-00000001 - P1: ERROR: Me Found"
weight: 10
categories: "issue"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Key: ISSUE-00000001
Priority: P1
Name: ERROR: Me Found
Creation: 2020-09-05
```

## TRIGGER
---
[Toehider - "I Like It!" (album launch live stream, 9/5/2020 @ 7:00AM-1:00PM Central Standard Time)](https://www.youtube.com/watch?v=_yhCcUzWjvk)

## RESOURCES
---
[SALTY'S SONG - Error Me Found.0.mp3](/static/audio/Salty'sSong-ErrorMeFound.0.mp3)

[![SALTY'S SONG - Error Me Found.0.jpg](/static/images/Salty'sSong-ErrorMeFound.0.jpg)](/static/images/Salty'sSong-ErrorMeFound.0.jpg)
[![SALTY'S SONG - Error Me Found.1.jpg](/static/images/Salty'sSong-ErrorMeFound.1.jpg)](/static/images/Salty'sSong-ErrorMeFound.1.jpg)

## PREDICTION
---
```
The Real Men are born.
```