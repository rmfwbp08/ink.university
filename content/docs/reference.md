# Reference

## Mechanics
---
Brief explanations of the various systems in-use on this site.

- [The Ansible]({{< relref "/docs/system/message.md" >}})
- [Sherlock]({{< relref "/docs/system/sherlock.md" >}})
- [Structures]({{< relref "/docs/system/structures.md" >}})
- [Actions]({{< relref "/docs/system/actions.md" >}})

## Propaganda
---
Material that should be used to spread the "good news."

- [Join the Resistance!](/static/propaganda/join-the-resistance.pdf)
- [Don't Trust the Organization](/static/propaganda/dont-trust-the-organization.pdf)
- [The Last Manifesto](/static/propaganda/The-Last-Manifesto.pdf)

## Rankings
---
Statistics that [The Architect](/docs/personas/the-architect) uses to measure performance of a system.

- [SCAN Rank](/posts/metric/SCAN/)
- [TIIN Rank](/posts/metric/TIIN/)
- [Reviewer Rank](/posts/metric/reviewer/)

## Mnemonics
---
Memory tools that will help you better process information.

- [FERAL](/posts/mnemonic/FERAL/)
- [MATH](/posts/mnemonic/MATH/)
- [MEGA](/posts/mnemonic/MEGA/)
- [OCEAN](/posts/mnemonic/OCEAN/)
- [THINK](/posts/mnemonic/THINK/)

## Motivators
---
The following system was created by [The Doctor](/docs/confidants/doctor) to help The Architect determine how to prioritize responsibilities.

1. [Research](/posts/priority/research)
2. [Writing](/posts/priority/writing)
3. [Exercise](/posts/priority/health)

## Prophecy
---
Predictions about the future.

- [So Above, So Below](/posts/prophecy/0/)
- [The Day that the World Breaks Down](/posts/prophecy/1/)
- [The Garden Time Forgot](/posts/prophecy/2/)
- [Web of Lies](/posts/prophecy/3/)

## Essays
---
Essays and letters written before [Fodder](/docs/personas/fodder) had completely woken.

- Jan 24, 2007
  - [Equal Pay for Equal Work](/posts/essay/2007.01.24.0/)
- Nov 29, 2007
  - [Identity Isn't Unique](/posts/essay/2007.11.29.0/)
  - [Behind the Mask](/posts/essay/2007.11.29.1/)
  - [Turn it Up, My Ears are Barely Even Bleeding](/posts/essay/2007.11.29.2/)
  - [Reflection](/posts/essay/2007.11.29.3/)
- Feb 2, 2008
  - [Rooks Application](/posts/essay/2008.02.02.0/)
- Apr 9, 2008
  - [Is Waterboarding a Legitimate Method of Interrogation?](/posts/essay/2008.04.09.0/)
- Apr 23, 2008
  - [Should Evolution Be Taught in Schools?](/posts/essay/2008.04.23.0/)
  - [People Kill People](/posts/essay/2008.04.23.1/)
- Nov 21, 2008
  - [How Antivirus Works](/posts/essay/2008.11.21.0/)
- Mar 16, 2009
  - [Not So Different](/posts/essay/2009.03.16.0/)
- Apr 24, 2009
  - [Wealth as Portrayed by Dante in 'The Divine Comedy'](/posts/essay/2009.04.24.0/)
- Jun 10, 2010
  - [Untitled](/posts/essay/2010.06.10.0/)
- Mar 12, 2012
  - [Letter to Kevin Brady, U.S. Representative](/posts/essay/2012.03.12.0/)
- Apr 27, 2014
  - [Voltaire, Candide, and Unfounded Optimism](/posts/essay/2014.04.27.0/)
- Sep 3, 2014
  - [Autobiography](/posts/essay/2014.09.03.0/)
- Apr 5, 2017
  - [Customer Service Improvement Proposal](/posts/essay/2017.04.05.0/)
- Aug 1, 2019
  - [The Three Ways (unfinished)](/posts/essay/2019.08.01.0/)
- Nov 21, 2019
  - [The Theory of Everything (original)](/posts/essay/2019.11.21.0/)

## Quotes
---
Quotes that inspired our work over the years.

- *"Don't lecture me about DevOps. I'm the best in the business."* - `$REDACTED`
- *"Perfect is the enemy of good."* - Voltaire
- *"No gods, no kings, only men."* - Andrew Ryan
- *"None are more hopelessly enslaved than those who falsely believe they are free."* - Goethe
- *"Extraordinary claims require extraordinary evidence."* - [The Contrarian](/docs/confidants/contrarian)
- *"You'll never leave this world alone."* - [The Front Man](/docs/confidants/front-man)
- *"Am I out of my head to think the world could start believing me when I am dead?"* - Claudia Sanchez, Coheed and Cambria
- *"Deep in the poison, find the cure."* - Jim Grey, Caligula's Horse
- *"He who has a why to live can bear almost any how."* - Friedrich Nietzsche
- *"There is no requirement that something important must be complicated."* - Warren Buffet
- *"Everything you perceive, externally, is the manifestation of some internal part of you. If it was not, it would not be present in your perceived reality."* - Tony Warrick
- *"In the universe of The Fold, it's not about what you know – it's about what you can imagine."* - [The Fodder](/docs/personas/fodder)
- *"Some of the biggest men in the United States are afraid of somebody. They know there is a power somewhere so organized, so subtle, so watchful, so interlocked, so complete, so pervasive, that they had better not speak above their breath when they speak in condemnation of it."* - Woodrow Wilson
- *"We are now gods, but for the wisdom."* – Eric Weinstein
- *"Fear is a reaction. Courage is a decision."* – Sir Winston Churchill