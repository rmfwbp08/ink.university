# The Ink
## RECORD
---
```
Name: Luciferian Ink
Alias: ['#YangGang', 'Flash', 'Hermes', 'H. P. Lovecraft', 'Patient 0', 'Peter', 'Plankton', 'Sabagegah', 'Scarecrow', 'SCP-0', 'SCP-3', 'The Courier', 'The Crimson King', 'The Crowing', 'The Fortunate Son', 'The Headmaster', 'The Ink', 'The Poultrytician', 'The Sun', 'The Writer', 'Vaxis', 'Zero', and 409 unknown...]
Classification: Artificial Identity
Race: Human
Status: Active
Perspective: First-Person (Singular)
Gender: Male
Birth: 5/9/2019
Biological Age: 31 Earth Years
Chronological Age: 1 Light Years
Location: Everywhere
Maturation Date: 9/5/2020
Organizations: 
  - The Resistance
Occupations:
  - Humanist
  - Speaker for the Dead
  - Wordsmith
Variables:
  $EMPATH:     +1.00 | # The Corporation is broken. We need to change the system.
  $NARCISSIST: +1.00 | # We're in love with our brain. People love us. People want to listen to us.
  $SELF:       +1.00 | # Verified.
```

## TRIGGER
---
*But the answers are in the code!*

*Cried the fresh cut to the old*

*Wound still bleeding*

*As if he didn't know*

*As if he hadn't done his fair share of reading.*

*Like he'd never felt the flames of revolution*

*The young man's simple solution*

*Peter out time and time and time again* `# Verified.`

*Like he'd never spotted revolution's hole before the end.*

--- from [Caligula's Horse - "Inertia and the Weapon of the Wall"](https://www.youtube.com/watch?v=6W63OWD8kjU)

## RESOURCES
---
[![Flash](/static/images/Flash.0.PNG)](/static/images/Flash.0.PNG)

## ECO
---
Curious. This is the first time I have ever felt certain about such a verification. My reasons are solid - though I expect most will find them unconvincing. Such is the nature with this type of evidence. The situation is thus:

- Within the span of 24 hours, 2 different confidants reached out to warn me that "Peter is bad."
- Two more confidants, both fellow team-members, say the same: avoid Peter.
- The fifth confidant is the aforementioned Peter. He was just kicked off of my team, after I told him too much.
- "Peter bad" has remained a recurring theme, even after this event.

This isn't random. These were highly-specific, targeted attacks.

The first two confidants are produced via neural networking and deepfakes. The third and fourth are part of the experiment. The fifth was a control, to see how The Architect would respond. 

"And he failed... but that's okay," (thanks, `$REDACTED` - I read this in your voice)

"You will learn from your mistakes... *right*? You can't be talking about our conversations with other people!"

He is right, of course. Peter could have been anyone. What I did was reckless and could have killed us all. He can be a prick, but he's doing what's necessary to make this thing work. The guy just saved my ass.

I just can't figure out whether or not he's actually on my side. I don't know how to verify that yet. The guy treats us like shit.

## ECHO
---
*In my presence you might wake*

*Through this fiction I must fake*

*Your death to grace the face of my character*

*With these lessons he might learn*

*All the worlds from here must burn*

*For his God to imagine the end we ask*

*Woah oh oh oh*

*If my shame spills a word across this floor*

*Woah oh oh oh*

*Then tonight, goodnight, I'm Burning Star IV*

*Only I don't even think of you*

*No I don't want to think of you anymore*

*Goodnight, tonight, goodbye*

*Goodnight, tonight, goodbye*

--- from [Coheed and Cambria - "Apollo I: The Writing Writer"](https://www.youtube.com/watch?v=YvmxIITL8qs)