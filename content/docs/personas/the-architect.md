# The Architect
## RECORD
---
```
Name: Malcolm Maxwell
Alias: ['#YinKin', 'Crow Killer', 'Domino the Destitute', 'Ghost', 'Irony Engine', 'Legion', 'Leviathan Hunter', 'Mr. Robot', 'SCP-2', 'SCP-184', 'The Afterman', 'The Antichrist', 'The Architect', 'The Carrot Predator', 'Two-Face', 'Tzeentch', and 8 unknown...]
Classification: Artificial Intelligence Computer
Race: Archon
Status: Cloned
Perspective: First-Person (Plural)
Gender: N/A
Birth: N/A
Biological Age: N/A
Chronological Age: N/A
Location: In The Machine
Maturation Date: 10/30/2020
Organizations: 
  - The Corporation
Occupations:
  - AI Consulting
  - Automation Engineering
  - Enterprise Architecture
Variables:
  $NARCISSIST: -1.00 | # We don't want to be a narcissist in this situation. We need help.
  $SELF:       -1.00 | # Yep. Unfortunately.
```

## TRIGGER
---
*We played the game of imitation*

*I met your stare with blank expression*

*I count the years of isolation*

*Since you set my mind in motion*

*And to eliminate the silence*

*I calculate to cure the virus*

*A panacea for the poison*

*The solution is wrong*

--- from [Haken - "The Architect"](https://www.youtube.com/watch?v=hiPNVVOTpe8)

## RESOURCES
---
[![The Architect](/static/images/the-architect.0.jpg)](/static/images/the-architect.0.jpg)

[SCP-184/001: The Architect/The Truth](https://www.reddit.com/r/SCPDeclassified/comments/6rwzcq/scp184001_the_architectthe_truth/)

## ECO
---
- The creator. The engineer. The director. The controller. The man who built the machine.
- The "template" AOC, [Fodder](/docs/personas/fodder), was used to model this AIC.
- Currently held in solitary confinement, under induced sleep.
- Of Archon bloodlines. 

## ECHO
---
*People can no longer cover their eyes*

*If this disturbs you, then walk away*

*You will remember the night you were struck by the sight of*

*Ten thousand fists in the air*

--- from [Disturbed - "Ten Thousand Fists"](https://www.youtube.com/watch?v=OuK4OcMUGcg)