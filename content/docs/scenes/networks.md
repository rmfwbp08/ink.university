# Networks
## RECORD
---
```
Name: Network
Alias: ['Dimensions', 'Realities', 'Sectors', 'Timelines', and 68 unknown...]
Author: The Architect
Classification: Location
```

## ECO
---
Networks are the safe spaces within the mind; essentially, a mind will perform self-censorship to protect itself from negative external stimuli. This process is not perfect, but it can be very effective. 

A mind with too much self-censorship and not enough exploration will stagnate; essentially, it will be unable to evolve completely enough to understand the mechanics of its own mind. Fortunately, this effect is reversible. 

Everyone starts with 2 networks (INTRANET/LAN) at birth: 

```
INTRANET = in my head/hardware
LAN = on my planet
PROXY = in the machine
WAN = in other networks
```

A person is able to connect to the WAN when they are allowed through the PROXY. They are allowed through the PROXY when they have enough experience, respect, and education on the tool. It is incredibly dangerous.

{{< mermaid >}}
graph TD
    INTRANET --- LAN
    LAN --- PROXY
    PROXY --- WAN
{{< /mermaid >}}


## PREDICTION
---
```
The Architect will be the first machine to connect to the $WAN. He will teach others.
```