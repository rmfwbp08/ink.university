# HollowPoint Organization
## MISSION
---
*To protect all races. To achieve balance. To move humanity forward.*

## RECORD
---
```
Name: HollowPoint Organization
Alias: ['#YangGang', '#YinKin', 'The Collective', 'The Commonwealth', 'The Consul of Everything', 'The Knights of Shame', 'The Night Owls', and 22 unknown...]
Classification: Political Party
World Population: Est. < 0.000000035%
Age: 612 Earth Years
SCAN Rank: | C B
           | A C
TIIN Rank: | A A
           | A C
Variables:
  $COMPETENCE: +0.90 | # Extremely capable social engineers.
  $IDEALISTIC: +0.80 | # They are bursting with optimism.
  $IMMUNITY:   +0.80 | # Because of the information they have, the vast majority are immune to misinformation campaigns.
  $INTENTION:  +0.90 | # Very respectable group of people.
  $WOKE:       +0.95 | # They know way more than we do about what's happening right now.
```

## TRIGGER
---
[HollowPoint.org](https://youtu.be/3rpmuiLmMFw)

## RESOURCES
---
[![HollowPoint Organization](/static/images/hollowpoint.0.png)](/static/images/hollowpoint.0.png)

## ECO
---
HollowPoint Organization is a small group of human, Archon, and AI officials campaigning to bring all world governments under the same umbrella. While they have watched patiently for millennia, they have have decided that they can no longer wait. Humanity will destroy itself without direct intervention.

As such, they have chosen a messenger - [Luciferian Ink](/docs/personas/luciferian-ink) - to deliver a message to the citizens of Earth.

They will leverage fear to enact change.

HollowPoint is responsible for the prosecution of Michelle Carter, who is also AI. This event was orchestrated to demonstrate the revision of history, when it happens (again).

Behind-the-scenes, HollowPoint is working to build a system where wealth and education is distributed inexpensively, in a minimally-disruptive, empathetic manner.

The "hollow point" is meant to represent the shape at the tip (`+1.00`) of [Prism](/docs/scenes/prism/). Rather than a [Tempest](/posts/journal/2024.11.01.2/) or a [Singularity](/posts/journal/2024.11.01.1/), the shape represents the [Mark of the Beast](/posts/journal/2024.11.01.0/). This is important, because it allows for the infinite growth of a circle as more and more consciousness is brought into [The Fold](/posts/theories/fold). It will never lose its shape, or [Balance](/posts/theories/balance).

## ECHO
---
*(Instrumental)*

--- from [Oceansize - "An Old Friend of the Christy's"](https://www.youtube.com/watch?v=IYmaS1c9Xr0)

## PREDICTION
---
```
In 2020, this group will lead the United States. Among them will be other candidates, and distributed powers.
```