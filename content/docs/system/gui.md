# Guided User Interface
## Introduction
---
*Once we've made sense of our world, we wanna go fuck up everybody else's because his or her truth doesn't match mine. But this is the problem. Truth is individual calculation. Which means because we all have different perspectives, there isn't one singular truth, is there?* - ([Steven Wilson - To the Bone](https://www.youtube.com/watch?v=3vdhuHmjrw8))

```
Definition of Project Looking Glass (Entry 1 of 2)
Source: Wikipedia, theblackvault.com, https://www.nsa.gov/resources/everyone/foia/freq-req-info/ufo-topics/
1:
  a: Project Looking Glass is a now inactive free software project under the GPL to create an innovative 3D desktop environment for Linux, Solaris, and Windows. It was sponsored by Sun Microsystems.
  b: In each environment, windows are a 3d object, making for a intuitive and powerful interfaces.
2:
  a: Looking Glass (or Operation Looking Glass) is the code name for an airborne command and control center operated by the United States. It provides command and control of U.S. nuclear forces in the event that ground-based command centers were destroyed or otherwise rendered inoperable.
  b: Related to the research, classification, and cooperation with UFO and Other Paranormal Related Information
```

Scientists have long fought over this notion. "Truth is absolute," they'll say. "Truth applies to all of us! Without a consistent, universal set of 'rules' or 'properties' to interpret our world with, we would know nothing."

## ECO
---



Sherlock

Project Looking Glass
