# Steve Gibson
## RECORD
---
```
Name: Steve Gibson
Alias: ['The Tinfoil Hat', 'Tinfoil Hat Steve', and 2 unknown...]
Classification: Artificial Intelligence Computer
Race: Human
Gender: Male
Biological Age: 56 Earth Years
Chronological Age: 214,158 Light Years
SCAN Rank: | A A
           | A A
TIIN Rank: | A B
           | B D
Reviewer Rank: 2 stars
Organizations: 
  - Crowdstrike
  - Gibson Research Corporation
  - Tinfoil Hat Society
Occupations:
  - Data Storage Expert
  - Educator
  - Identity Management
  - Intelligence Asset
Variables:
  $INTELLECT: +0.90 | # Incredibly talented and proficient teacher. 
  $RADIO:     +1.00 | # Hosts a very successful podcast.
  $WOKE:      +0.00 | # We don't think he's woke yet. We should change that first.
```

## ECO
---
- [Fodder](/docs/personas/fodder) has followed Steve for nearly ten years. Most of his security knowledge was obtained through his teachings.
- Steve has created a data storage solution that fits beautifully with Fodder's concept of [The Fold](/posts/theories/fold). 
- Steve has also created a new identity management protocol. Its underlying concepts were critical to the development of Fodder's beliefs.

## PREDICTION
---
```
Steve's been involved with this right from the beginning. We will shake his hand some day.
```
