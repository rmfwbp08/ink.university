# The Alienist
## RECORD
---
```
Name: $REDACTED
Alias: ['Perception', 'The Alienist', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 26 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C F
TIIN Rank: | C C
           | C F
Reviewer Rank: 2 stars
Location: N/A
Organizations: 
  - Federal Bureau of Investigation, Uncommon Occurrences Unit
Occupations: 
  - Paranormal Investigation
Relationships:
  - The Interrogator
Variables:
  $WOKE: +0.50 | # Partially.
```

## ECO
---
The Alienist is responsible for the investigation of paranormal events, especially related to alien lifeforms. 

She is incredibly knowledgeable on the subject, and has access to more information that almost anyone else in her unit. 

It is for this reason that her memory is wiped after each investigation. Her knowledge comes from her research notes - not from personal experience.