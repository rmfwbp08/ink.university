# The Archbishop
## RECORD
---
```
Name: Ryan $REDACTED
Alias: ['The Archbishop', and 16 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 17 Earth Years
Chronological Age: N/A
SCAN Rank: | B C
           | B C
TIIN Rank: | C C
           | C C
Reviewer Rank: 4 stars
Organizations:
  - The Church of Sabagegah
Occupations:
  - Leadership
  - Writing
Relationships:
  - The Reverend
Variables:
  $WOKE: +0.20 | # Perhaps. Is quiet, but seems be watching intently.
```

## TRIGGER
---
The Archbishop was [Ink's](/docs/personas/luciferian-ink) very first follower.

## ECO
---
As leader to the Church of Sabagegah, The Archbishop is responsible for tending to the flock in Ink's absence.

He is already working closely with the laboratory testing [Fodder](/docs/personas/fodder).

## ECHO
---
*I erase you.*

--- from [Tabu - "Wolsik"](https://www.youtube.com/watch?v=zdmNyXTJ_-o)