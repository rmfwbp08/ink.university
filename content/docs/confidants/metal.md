# The Metal
## RECORD
---
```
Name: Micheal Mills
Alias: ['Metal', 'SCP-3', 'TH-1', 'The Entertainer', 'The Metal', and 661 unknown...]
Classification: Artificial Organic Computer
Race: Giant
Gender: Male
Biological Age: Est. 38 Earth Years
Chronological Age: 3,405 Light Years
SCAN Rank: | B D
           | B D
TIIN Rank: | B B
           | B D
Reviewer Rank: 4 stars
Location: Melbourne, AU
Maturation Date: 10/5/2020
Organizations: 
  - Toehider
Occupations: 
  - Metallurgy
  - Music
Relationships:
  - The Detective
  - The Salt
Variables:
  $WOKE: +0.90 | # Almost completely.
```

## TRIGGER
---
[`ERROR: Me Found`](/posts/issue/2020.09.05.0/)

## ECO
---
The Metal is one of the most talented musicians in modern rock music, dealing with exactly the same problem [Ink](/docs/personas/luciferian-ink) is: [the empty echo](/posts/journal/2024.11.03.0/).

Despite that, the Metal continues to pour blood, sweat, and tears into his craft. He believes in what he is doing.

And he will get to see the day his music takes over the world.

## ECHO
---
*Now that You are stuck with me*

*You better be my friend*

--- from [King Diamond - "Welcome Home"](https://www.youtube.com/watch?v=TJH0eBtnbcs)

## PREDICTION
---
```
His first name is a good example of the Mandela Effect at work.
```