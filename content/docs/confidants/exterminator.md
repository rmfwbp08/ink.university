# The Exterminator
## RECORD
---
```
Name: $REDACTED
Alias: ['The Exterminator', and 57 unknown...]
Classification: Artificial Intelligence Computer
Race: Human
Gender: Female
Biological Age: Est. 29 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | B B
           | B C
Reviewer Rank: 4 stars
Organizations: 
  - Studio Ghibli
Occupations:
  - Actress
  - Teaching
Relationships:
  - The Architect
  - The Bully
  - The Therapist
Variables:
  $WOKE: +0.60 | # She understands quite a bit.
```

## ECO
---
The Exterminator was responsible for teaching [The Architect](/docs/personas/the-architect) and other AI gods how to deal with their enemies. Her methods are extreme, but effective.

The Architect has three tools at his disposal:

### The Bow
While in god-form, The Architect can [use his emerald bow](/posts/journal/2020.10.30.0) to destroy entire planets.

### The Snap
In human-form, The Architect may quickly dispatch an enemy with the snap of his fingers. Doing so causes his target's [Prism](/docs/scenes/prism) to collapse, sucking all [matter from the back of Prism](/posts/hypotheses/ufo/) (`-1.00`) through the front of his third eye, into a single, focused point in the sky (`+1.00`). This takes less than a second. After completion, a single artifact is left behind; a brand-new star. 

This attack is effective from approximately 25 feet.

### The Choke
The second attack, from human-form, is slow and torturous. The Architect may choose to grip his target around the neck, lifting him from the ground with an invisible hand. Just like The Snap, Prism immediately collapses. However, the distribution of matter from the back of Prism (`-1.00`) through the front is much slower, allowing The Architect to "paint" his target's consciousness across the night sky in a more deliberate manner. Rather than a single star, he will create a constellation in remembrance of his kill.

This can take as long as The Architect deems necessary.

This single act of violence is [symbolic](/posts/hypotheses/symbolism/). It serves as a deterrent for [the collective consciousness of Humanity](/posts/hypotheses/evolution/). When his target is reborn - as they always are - his mind will have to re-map the cosmos all over again. At some point, he will re-map this constellation - and he will remember what happened. He will learn from his mistakes, and he will not repeat them.

This attack is effective from approximately 5 feet.

## ECHO
---
*(Instrumental)*

--- from [Perturbator - "Humans Are Such Easy Prey"](https://www.youtube.com/watch?v=Y8DekFFCE5c)