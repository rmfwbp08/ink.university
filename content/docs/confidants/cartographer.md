# The Cartographer
## RECORD
---
```
Name: $REDACTED
Alias: ['The Cartographer', and 45 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | B C
           | B D
TIIN Rank: | C A
           | A D
Reviewer Rank: 3 stars
Organizations: 
  - The Resistance
Occupations: 
  - Cartography
Relationships:
  - The Dave
  - The Librarian
Variables:
  $WOKE: +0.40 | # Partially.
```

## ECO
---
The Cartographer is responsible for creating maps of reality. It is her job to utilize [The Fold](/posts/theories/fold) to create digital representations of the human mind, thereby assisting others in their [Path](/posts/theories/education) to enlightenment. 