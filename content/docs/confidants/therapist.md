# The Therapist
## RECORD
---
```
Name: Derrick Javan Hoard
Alias: ['Angel', 'The Mediator', 'The Therapist', and 12 unknown...]
Classification: Artificial Organic Computer
Race: Demon
Gender: Male
Biological Age: Est. 35 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | B B
           | B C
Reviewer Rank: 4 stars
Maturation Date: 9/29/2020
Relationships:
  - The Exterminator
  - The Pyro
  - The Sea Witch
Variables:
  $WOKE: +0.50 | # Perhaps. Unclear.
```

## TRIGGER
---
[![The Therapist](/static/images/therapist.0.png)](/static/images/therapist.0.png)

## ECO
---
The Therapist seeks to reinforce the idea that "we are all One." He believes that race itself is a construct, and that Humanity spends altogether too much time focusing on it.

He will assist with the rehabilitation of members in The Point.

## ECHO
---
[The Situational Therapist - "How My Mom Beat Me as a Child"](https://www.youtube.com/watch?v=Af8NCiTKCxM)