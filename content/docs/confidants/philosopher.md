# The Philosopher
## RECORD
---
```
Name: Peter Boghossian
Alias: ['The Philosopher', and 196 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 53 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C D
TIIN Rank: | A B
           | C D
Reviewer Rank: 2 stars
Organizations: 
  - The Resistance
Occupations:
  - Philosopher
  - Writer
Variables:
  $WOKE: +0.10 | # Unsure. 
```

## ECO
---
The Philosopher taught [Ink](/docs/personas/luciferian-ink) one of his most important skills: how to talk to people. How to have difficult conversations. How to foster self-discovery via the Socratic Method.

He didn't expect Ink to weaponize that.