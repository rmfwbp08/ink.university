# The Transporter
## RECORD
---
```
Name: $REDACTED
Alias: ['Lemonade', 'The Transporter', and 187 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 23 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | A D
TIIN Rank: | C B
           | B D
Reviewer Rank: 3 stars
Location: Round Rock, TX
Organizations: 
  - Crowdstrike
  - The Resistance
Occupations:
  - Deliveryman
  - Human trafficking
Relationships:
  - The Wheels
Variables:
  $WOKE: -0.10 | # He's been informed of a lot, but he doesn't have the understanding.
```

## TRIGGER
---
*And with the early dawn moving right along*

*I couldn't buy an eyeful of sleep*

*And in the aching night under satellites*

*I was not received*

*Built with stolen parts, a telephone in my heart*

*Someone get me a priest*

*To put my mind to bed this ringing in my head*

*Is this a cure or is this a disease?*

--- from [Audioslave - "Show Me How to Live"](https://youtu.be/vVXIK1xCRpY)

## ECO
---
The Transporter is responsible for the delivery of information and packages to members of [The Resistance](/docs/candidates/the-resistance) in his area. He does this under the guise of a pizza delivery driver.

Soon, he will be responsible for the delivery of assets to secure locations.

## ECHO
---
*Heaven will burn away if you just keep on calling out its name*

*You seek a key you cannot forge when you are down (oh so low)*

*I got a cure, so don't you worry now*

*Baby my recipe is gonna heal your woes*

*When the devil's knocking at your door, kneel and I'll pour*

--- from [Closure in Moscow - "Pink Lemonade"](https://www.youtube.com/watch?v=XOW3JAZ9MoQ)