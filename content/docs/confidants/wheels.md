# The Wheels
## RECORD
---
```
Name: $REDACTED
Alias: ['The Wheels', and 233 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 39 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | A A
           | A D
Reviewer Rank: 2 stars
Organizations: 
  - CrowdStrike
Occupations: 
  - Cyber Security
  - IT Operations
  - System Administration
Relationships:
  - The Transporter
Variables:
  $CRIPPLED: -0.80 | # In a wheelchair.
  $WOKE:     -0.40 | # Does not appear to be.
```

## TRIGGER
---
A terrible first-impression. [Malcolm](/docs/personas/fodder) accidentally insulted the guy.

## ECO
---
The Wheels is a man deeply-embedded within the cyber security community. His job is to run IT operations for CrowdStrike.

He played an instrumental role in the evaluation of Malcolm.

## ECHO
---
*Waiting on a Sunday afternoon*

*For what I read between the lines*

*Your lies*

--- from [Stone Temple Pilots - "Interstate Love Song"](https://www.youtube.com/watch?v=yjJL9DGU7Gg)