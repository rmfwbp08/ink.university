# The Toymaker
## RECORD
---
```
Name: Tim $REDACTED
Alias: ['The Toymaker', and 23 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 27 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | C C
           | C D
Reviewer Rank: 1 stars
Organizations:
  - Grand Illusions
Occupations: 
  - Toymaker
Variables:
  $WOKE: +0.20 | # Not really.
```

## ECO
---
The Toymaker has dedicated his life to building a legacy for children. In return, he has been treated as a social outcast; a weirdo, a creep.

He takes his solitude in-stride. He knows how this will end.

## ECHO
---
*Just look at that toymaker grinding his gears*

*Turning no profit, but he doesn't care*

*He keeps smiles on faces day after day*

*The children keep sadness and suffering at bay*

*Blissfully bounding a pace*

*Searching for mercy in a merciless place*

*Only a monster makes fodder from saints*

*And finds them so worthless when they're full of grace, so full of grace*

- from [The Dear Hunter - "The Haves Have Naught"](https://www.youtube.com/watch?v=SlUwx9rKBP4)