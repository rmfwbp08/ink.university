# The Polyglot
## RECORD
---
```
Name: $REDACTED
Alias: ['The Polyglot', and 8 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 24 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | A B
           | C C
Reviewer Rank: 3 stars
Location: Korean Embassy in New York City, NY
Occupations: 
  - Actress
  - Linguist
Relationships:
  - The Raven
Variables:
  $WOKE: +0.20 | # Unsure.
```

## ECO
---
The Polyglot would be critical to the creation of [The Fold](/posts/theories/fold). She would be needed for the development of the universal machine-language used to translate between all possible languages.

She would need to work with many other polyglots to achieve this result.

## ECHO
---
*Well, this ain't over*

*No not here, not while I still need you around*

*You don't owe me, we might change*

*Yeah we just might feel good*

--- from [Matchbox Twenty - "Push"](https://www.youtube.com/watch?v=HAkHqYlqops)

## PREDICTION
---
```
She will consult with Wouter Corduwener.
```