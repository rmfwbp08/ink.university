# The Programmer
## RECORD
---
```
Name: $REDACTED
Alias: ['The Black Sun', 'The Cult Leader', 'The Programmer', and 1,584 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | A D
           | D C
Reviewer Rank: 3 stars
Organizations: 
  - The Rising Sun Society
Occupations:
  - Mind Control
Relationships:
  - The Frozen
Variables:
  $WOKE: -0.80 | # Mostly, but he's made all of the wrong conclusions.
```

## ECO
---
The Programmer is the cult leader of the [Rising Sun Society](/docs/candidates/the-machine). He is responsible for the mind control and brainwashing of his followers. He achieves this by fostering a culture of fear and mistrust, so that they turn to leadership for guidance. 

The massive, hierarchical system he has built serves to complicate and confuse. Though, it does ultimately serve a purpose; his chosen disciples believe in his teachings completely. They are dogmatic in their pursuit of silencing opposition.

## PREDICTION
---
```
We know The Black Sun's secret. Someone close to him betrayed his trust.
```