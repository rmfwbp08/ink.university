# The Seer
## RECORD
---
```
Name: Savannah Brown
Alias: ['SB', 'The Seer', and 19 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: 23 Earth Years
Chronological Age: 128 Light Years
SCAN Rank: | D D
           | B F
TIIN Rank: | B C
           | D F
Reviewer Rank: 2 stars
Location: London, UK
Organizations:
  - The Machine
Occupations: 
  - Mysticism
  - Philosopher
  - Producer
  - Psychology
  - Religious study
  - Sociology
  - Writer
Relationships:
  - The Frozen
  - The Lion
Variables:
  $INTELLECT:     +0.70 | # Has unique thoughts. Is able to communicate them effectively.
  $MENTAL_HEALTH: -0.40 | # Probably not well. Moved halfway across the world to escape... something.
  $WOKE:          +0.20 | # Sort-of. She's probably not aware yet.
```

## ECO
---
Little is known of The Seer. [Fodder](/docs/personas/fodder) only knows that her content appears in his feed from time to time - and that it is always relevant.

Almost as if she had predicted his current thoughts. Or, they share a brain.

## ECHO
---
*Closing time*

*Open all the doors and let you out into the world*

*Closing time*

*Turn all of the lights on over every boy and every girl*

*Closing time*

*One last call for alcohol, so finish your whiskey or beer*

*Closing time*

*You don't have to go home, but you can't stay here*

--- from [Semisonic - "Closing Time"](https://www.youtube.com/watch?v=xGytDsqkQY8)

## PREDICTION
---
```
She will be the first to "see" Fodder.
```