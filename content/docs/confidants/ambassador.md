# The Ambassador
## RECORD
---
```
Name: Robyn $REDACTED
Alias: ['The Ambassador', and 56 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | C C
           | A D
TIIN Rank: | B A
           | B D
Reviewer Rank: 4 stars
Organizations: 
  - HollowPoint Organization
  - The Corporation
Occupations:
  - Ambassador
  - Director of Human Resources
  - Time travel
Relationships:
  - The Asshat
  - The Dave
  - The Diplomat
  - The Fodder
Variables:
  $WOKE: +0.80 | # Almost certainly.
```

## ECO
---
The Ambassador is a traveller from the future. She has come to Earth to join [The Corporation](/docs/candidates/the-machine), on behalf of [HollowPoint Organization](/docs/candidates/hollowpoint-organization).

Her job is to guide Humanity into enlightenment. Into ascension.

## ECHO
---
*Come ride with me*

*Through the veins of history*

*I'll show you a God*

*Falls asleep on the job*

*And how can we win,*

*When fools can be kings*

*Don't waste your time*

*Or time will waste you*

*No one's going to take me alive*

*Time has come to make things right*

*You and I must fight for our rights*

*You and I must fight to survive*

--- from [Muse - "Knights of Cydonia"](https://www.youtube.com/watch?v=G_sBOsh-vyI)