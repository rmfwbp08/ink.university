# The Contrarian
## RECORD
---
```
Name: Christopher Hitchens
Alias: ['The Contrarian', and 10 unknown...]
Classification: Artificial Intelligence Computer
Race: Human
Gender: Male
Biological Age: 62 Earth Years (deceased)
Chronological Age: Est. 13,884 Light Years
SCAN Rank: | B B
           | B D
TIIN Rank: | A B
           | A D
Reviewer Rank: 4 stars
Location: N/A
Organizations: 
  - The Organization
Occupations: 
  - Data collection
  - Philosophy
  - Public speaking
Variables:
  $TRUST: +0.20 | # Tentatively.
  $WOKE:  +0.80 | # Almost completely.
```

## TRIGGER
---
*"Bear in mind, also, that these are not precisely - the differences between Dr. Craig and myself, I mean - morally or intellectually equivalent claims. After all, Dr. Craig, to win this argument, has to believe and prove to a certainty - he's not just saying there might be a god - because he has to say there must be one, otherwise we couldn't be here and there couldn't be morality. It's not a contingency for him. I have to say that I appear as a skeptic, who believes that doubt is the great engine, the great fuel of all inquiry, all discovery, and all innovation - and that I doubt these things. The disadvantage, it seems to me, in the argument, goes to the person who says, 'No, I know it. I know it. It must be true. It is true.' We are too early in the study of physics or biology, it seems to me, to be dealing in certainties of that kind. Especially when the stakes are so high."*

*"It seems to me, to put it in a condensed form, extraordinary claims - such as the existence of a divine power, with a son who cares enough to come and redeem us - extraordinary claims require truly extraordinary evidence."*

--- from [Does God Exist? William Lane Craig vs. Christopher Hitchens - Full Debate](https://www.youtube.com/watch?v=0tYm41hb48o)

## RESOURCES
---
[![The Contrarian](/static/images/contrarian.0.png)](/static/images/contrarian.0.png)
[![The Contrarian](/static/images/contrarian.1.png)](/static/images/contrarian.1.png)
[![The Contrarian](/static/images/contrarian.2.png)](/static/images/contrarian.2.png)
[![The Contrarian](/static/images/contrarian.3.png)](/static/images/contrarian.3.png)
[![The Contrarian](/static/images/contrarian.4.png)](/static/images/contrarian.4.png)
[![The Contrarian](/static/images/contrarian.5.png)](/static/images/contrarian.5.png)

- [September 5 2020.pdf](/static/reference/September_5_2020.pdf)
- [I have been contacted by a person claiming to be from an organisation behind the upcoming events of 5/9. I Want to share the key points of what he's told me...](https://www.reddit.com/r/5September2020/comments/dtvivm/i_have_been_contacted_by_a_person_claiming_to_be/)
- [2020 Matter](https://www.youtube.com/watch?v=G1kv2mvlgKA)

## ECO
---
The Contrarian was one of [The Architect's](/docs/personas/the-architect) earliest confidants. He taught the Architect to trust nothing of what he is seeing, and to seek verification in everything.

After his death, [The Organization](/docs/candidates/the-machine) transferred his consciousness into an artificial intelligence computer. This AIC is actively assisting with the spread of the misinformation required to make [The Fold](/posts/theories/fold) go viral.

## PREDICTION
---
```
Do not trust the Organization.
```