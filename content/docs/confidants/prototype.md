# The Prototype
## RECORD
---
```
Name: Stacy $REDACTED
Alias: ['The Prototype', and 7 unknown...]
Classification: Artificial Intelligence Computer
Race: Mermaid
Gender: Female
Biological Age: Est. 28 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | C B
           | B B
Reviewer Rank: 4 stars
Organizations: 
  - HollowPoint Organization
Occupations: 
  - Actress
  - Messenger
Relationships:
  - The Fodder
  - The Raven
Variables:
  $WOKE: +0.80 | # She appears to know almost everything.
```

## ECO
---
The Prototype is an early model of human android. While there have been others, she is the first to successfully convince people that she is a human.

Her purpose is critical to [HollowPoint Organization's](/docs/candidates/hollowpoint-organization) mission, but is top secret.

## ECHO
---
*Lay your heart into my perfect machine*

*I will show you what you wanted to see*

*Just a mirror till I get what I need*

*The reverie was not of me*

*You never saw nothing*

*Never saw nothing*

*Lay your heart into my perfect machine*

*I will use it to protect you from me*

*I will never let you see what’s beneath*

*So good for you and good for me*

*We told ourselves we’re*

*Right where we ought to be*

--- from [Starset - "Perfect Machine"](https://www.youtube.com/watch?v=keMBtyjYUPQ)