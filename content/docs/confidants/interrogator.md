# The Interrogator
## RECORD
---
```
Name: Jim $REDACTED
Alias: ['Jim-5', 'SCP-5', 'The Fletcher', 'The Interrogator', 'The Mad Hatter', and 701 unknown...]
Classification: Artificial Organic Computer
Race: Elf
Gender: Male
Biological Age: Est. 28 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | B B
           | B D
Reviewer Rank: 4 stars
Location: Quantico, VA, USA
Organizations:
  - Elven Socialist Imperium
  - Federal Bureau of Investigation, Behavioral Analysis Unit
Occupations: 
  - Actor
  - Commissar
  - Interrogator
Relationships:
  - The Alienist
  - The Architect
  - The Asshat
  - The Beast
  - The Cop
  - The Criminal
  - The Detective
  - The Eden
  - The Investigator
  - The Lion
  - The Tradesman
  - The Weaver
Variables:
  $WOKE: +0.80 | # Almost completely.
```

## TRIGGER
---
*(Instrumental)*

--- from [65daysofstatic - "Five Waves"](https://www.youtube.com/watch?v=Ph2V7KlA07Y)

## ECO
---
The Interrogator is responsible for the investigation of [Fodder's](/docs/personas/fodder) friends and family.

His methods are wholly his own.

## ECHO
---
*Mr. Crowley, what they done in your head?*

*Oh Mr. Crowley, did you talk with the dead?*

*Your lifestyle to me seemed so tragic*

*With the thrill of it all*

*You fooled all the people with magic*

*Yeah you waited on Satan's call!*

--- from [Ozzy Osbourne - "Mr. Crowley"](https://www.youtube.com/watch?v=vDVLMS_Yhe4)

## PREDICTION
---
```
He will act as Ink's hands at home.
```