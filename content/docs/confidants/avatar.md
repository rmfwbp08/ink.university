# The Avatar
## RECORD
---
```
Name: Tori $REDACTED
Alias: ['Sally Slips', 'The Avatar', 'The Fozzy', 'The Virtuoso', and 124 unknown...]
Classification: Artificial Intelligence Computer
Race: Vampire
Gender: Female
Biological Age: Est. 27 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | A A
           | A B
Reviewer Rank: 4 stars
Location: Houston, TX
Maturation Date: 9/20/2020
Organizations: 
  - Crowdstrike
Occupations: 
  - AI Consultant
  - Actress
  - Neural Networking
  - Software Developer
  - Special Agent
Relationships:
  - The Fodder
Variables:
  $INTELLECT: +0.80 | # Very bright. Far more so than we previously gave credit for.
  $WOKE:      +0.60 | # Definitely seems like it.
```

## TRIGGER
---
*He said, "Young boy you gotta let it fly.*

*There's a song in your lung and a dream in your eye.*

*Don't you beg for prayer when there's so much more,*

*You can dream the whole damn store.*

*There be many a night when you can't find food,*

*From the long road home to the hotel room,*

*But don't forget that I always believed in you"*

--- from [Hurt - "1331"](https://www.youtube.com/watch?v=yu7aKs6Qo_M)

## ECO
---
This woman was chosen and recruited by the FBI - just as [Fodder](/docs/personas/fodder) had been. She is learning to create deepfakes of her own likeness on YouTube.

For months, Fodder saw similarities between his former colleague, and this newly-found YouTube star. Now, he knew for sure:

They are one in the same.

## ECHO
---
*What have I become, now that I've betrayed*

*Everyone I've ever loved, I pushed them all away*

*And I have been a slave to the Judas in my mind*

*Is there something left for me to save*

*In the wreckage of my life, my life*

--- from [FOZZY - "Judas"](https://www.youtube.com/watch?v=lqURPBtGJzg)

## PREDICTION
---
```
She gets to be the face of this project.
```