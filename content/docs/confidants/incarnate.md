# The Incarnate
## RECORD
---
```
Name: Jeffrey Epstein
Alias: ['Fixer', and 2 unknown...]
Classification: Incarnate
Race: Human
Gender: Male
Biological Age: 67 Earth Years
Chronological Age: 2,584,000 Light Years
SCAN Rank: | C F
           | D F
TIIN Rank: | A D
           | F F
Reviewer Rank: 3 stars
Organizations: 
  - The Corporation
Occupations: 
  - Human Trafficking
  - Lawyer
  - Manipulation
Variables:
  $DEPRAVITY: -1.00 | # Definitely depraved.
  $WOKE:      +1.00 | # Definitely woke.
```

## ECO
---
Jeffrey is the most morally-bankrupt, corrupt, and evil man to have ever walked the planet. His apathy towards humanity knows no bounds. His abuse of women and children is second to none. His ability to persuade, escaping justice time and again, is unmatched. Jeffrey is the single most powerful man on Earth. The single most feared. The most dangerous.

At least, in [Fodder's](/docs/personas/fodder) mind, he is. Reality is often more fuzzy.

And now it doesn't matter. Jeffrey was murdered in his sleep. Only his story, and the story of one thousand accusers lives on.

### About Incarnates
Incarnates are the mind's representation of its own worst thoughts, feelings, and behaviors. Looking upon an Incarnate typically causes feelings of revulsion and angst within the beholder. 

Despite that, it is important to note that your Incarnate may be biased in the negative direction. How an individual represents another in his/her mind very rarely matches the person's own beliefs about themselves, in reality. Your Incarnate is the worst, least-charitable interpretation of an identity that your mind is able to conceive of.

## ECHO
---
*Some will learn, many do*

*Cover up or spread it out*

*Turn around, had enough*

*Pick and choose, or pass it on*

*Buying in, heading for*

*Suffer now or suffer then*

*It's bad enough, I want the fear*

*Need the fear*

*'cause he's alone*

*(the fear has become)*

*He's alone*

*(and fear has become)*

--- from [Chevelle - "Vitamin R (Leading Us Along)"](https://www.youtube.com/watch?v=XEH7fw298CM)

## PREDICTION
---
```
Jeffrey's story will see an ending. One that will end human exploitation forever.
```
