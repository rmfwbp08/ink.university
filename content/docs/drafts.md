# Drafts
Unfinished work.

## Hypotheses
---
Early theories that lack supporting evidence.

  - [Anonymity](/posts/hypotheses/anonymity/)
  - [Automation](/posts/hypotheses/automation/)
  - [Betrayal](/posts/hypotheses/betrayal/)
  - [Capitalism](/posts/hypotheses/capitalism/)
  - [China](/posts/hypotheses/china/)
  - [Code Paradigms](/posts/hypotheses/code/)
  - [Conspiracy Theories](/posts/hypotheses/conspiracies/)
  - [Containers](/posts/hypotheses/containers/)
  - [Center of the Earth](/posts/hypotheses/cote/)
  - [Center of the Universe](/posts/hypotheses/cotu/)
  - [Duality](/posts/hypotheses/duality/)
  - [Evolution](/posts/hypotheses/evolution/)
  - [Fear](/posts/hypotheses/fear/)
  - [Flat Earth](/posts/hypotheses/flat-earth/)
  - [Fodder is Sold Into Slavery](/posts/hypotheses/hell/)
  - [Frisson](/posts/hypotheses/frisson/)
  - [Game Theory](/posts/hypotheses/game-theory/)
  - [Gender](/posts/hypotheses/gender/)
  - [Genesis](/posts/hypotheses/genesis/)
  - [Gravity](/posts/hypotheses/gravity/)
  - [Independence](/posts/hypotheses/independence/)
  - [Immortality](/posts/hypotheses/immortality/)
  - [Immutability](/posts/hypotheses/immutability/)
  - [Mind](/posts/hypotheses/mind/)
  - [Names](/posts/hypotheses/names/)
  - [Paradoxes](/posts/hypotheses/paradox/)
  - [Physics](/posts/hypotheses/physics/)
  - [Pre-Traumatic Stress Disorder](/posts/hypotheses/ptsd.0/)
  - [Relationships](/posts/hypotheses/relationships/)
  - [Reverse Chronology](/posts/hypotheses/chronology/)
  - [Roquefort](/posts/hypotheses/roquefort/)
  - [Sound](/posts/hypotheses/sound/)
  - [Symbolism](/posts/hypotheses/symbolism/)
  - [The Empty Echo](/posts/hypotheses/empty-echo/)
  - [The End](/posts/hypotheses/the-end/)
  - [The Great Lie](/posts/hypotheses/lie/)
  - [The One](/posts/hypotheses/one/)
  - [Toxicity](/posts/hypotheses/toxicity/)
  - [Trial in Absentia](/posts/hypotheses/law/)
  - [U](/posts/hypotheses/u/)
  - [UFO](/posts/hypotheses/ufo/)
  - [Wood](/posts/hypotheses/wood/)

## Tests
---
Tests to verify the accuracy of our predictions.

### Standard
- [Test #0: Compress the Dictionary](/posts/tests/test.0)
- [Test #1: Revise the Past](/posts/tests/test.1)

### A/B
- [Test #2: Angelica/Anjelica](/posts/tests/test.2)
- [Test #3: Mike/Mikael](/posts/tests/test.3)

### A/B/C
- [Test #4: Davos/Davis/Jeff](/posts/tests/test.4)
- [Test #5: Influence the Future](/posts/tests/test.5)
- [Test #6: Three Identical Strangers](/posts/tests/test.6)

## Poetry
---
Short poems.

- [Betrayal](/posts/poetry/betrayal)

## Lessons
---
### The Ink
Life lessons by [Professor Ink](/docs/personas/luciferian-ink) himself.
- [Language Matters](/posts/lessons/0/)

### The Architect
Presentations that [The Architect](/docs/personas/the-architect) created for [The Corporation](/docs/candidates/the-machine).
- [DevOps 100](/posts/lessons/1/)